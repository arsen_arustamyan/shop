<?php
    session_start();

    $url = "http://".$_SERVER['HTTP_HOST'];

    if ($_SERVER['REQUEST_URI'] == '/' || $_SERVER['REQUEST_URI'] == '/index.php') {
        require_once ('webapp/service/CategoryService.php');
    } else {
        require_once ('../webapp/service/CategoryService.php');
    }

    $categoryService = new CategoryService();
?>
<div class="container-fluid">
    <div class="row">
        <header id="header">
            <div class="container-fluid">
                <div class="row">

                    <!--Navbar-->
                    <div class="top-nav">
                        <nav class="navbar navbar-dark bg-info">

                            <!-- Collapse button-->
                            <button class="navbar-toggler hidden-sm-up" type="button" data-toggle="collapse"
                                    data-target="#collapseEx2">
                                <i class="fa fa-bars"></i>
                            </button>

                            <div class="container">

                                <!--Collapse content-->
                                <div class="collapse navbar-toggleable-xs" id="collapseEx2">
                                    <!--Navbar Brand-->
                                    <a href="/" class="navbar-brand">Shop</a>
                                    <!--Links-->
                                    <ul class="nav navbar-nav">
                                        <? foreach ($categoryService->getAllCategory() as $category): ?>
                                        <li class="nav-item">
                                            <a href="<?= $url?>/pages/category.php?category=<?= $category['category_name']?>" class="nav-link"><?= $category['category_name']?></span></a>
                                        </li>
                                        <? endforeach; ?>
                                        <li class="nav-item">
                                            <a class="nav-link" data-toggle="modal" data-target="#modal-developer">Разработчик</a>
                                        </li>
                                    </ul>
                                    <!--Search form-->
                                    <div class="auth-profile">
                                        <ul class="nav navbar-nav">
                                            <li class="nav-item">
                                                <? if (!$_SESSION['login']): ?>
                                                    <a class="nav-link" data-toggle="modal" data-target="#modal-login"><i
                                                            class="fa fa-user" aria-hidden="true"></i> Войти</span></a>
                                                <? endif; ?>
                                                <? if ($_SESSION['login']): ?>
                                                    <a href="<?= $url ?>/pages/profile.php" class="nav-link"><i
                                                            class="fa fa-user" aria-hidden="true"></i> <?= $_SESSION['login'] ?></span></a>
                                                <? endif; ?>
                                            </li>
                                            <li class="nav-item">
                                                <? if (!$_SESSION['login']): ?>
                                                    <a class="nav-link" data-toggle="modal" data-target="#modal-register">Регистрация</a>
                                                <? endif; ?>
                                                <? if ($_SESSION['login']): ?>
                                                    <a class="nav-link exit">Выйти</a>
                                                <? endif; ?>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <!--/.Collapse content-->

                            </div>

                        </nav>
                    </div>
                    <!--/.Navbar-->
                </div>
            </div>

            <div class="container">
                <div class="row">
                    <a href="<?= $url ?>/pages/shopingcart.php">
                        <div class="col-sm-2 cart bg-info hoverable">
                            <div class="cart-page">
                                <div class="cart-info">
                                    <? if ($_SESSION['login']): ?>
                                        <span class="cart-price"><?= $profileService->getSumma() ?> руб.</span>
                                        <span class="count-product"><?= $profileService->getCount() ?> предмет(а)</span>
                                    <? endif; ?>
                                    <? if (!$_SESSION['login']): ?>
                                        <span class="cart-price">0 руб.</span>
                                        <span class="count-product">0 предмет(а)</span>
                                    <? endif; ?>
                                </div>
                                <div class="cart-icon">
                                    <img src="<?= $url ?>/img/cart-icon.png" alt="">
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        </header>
    </div>
</div>


<div class="modal fade modal-ext" id="modal-login" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <!--Content-->
        <div class="modal-content">

            <!--Header-->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h3><i class="fa fa-user"></i> Авторизация</h3>
            </div>

            <!--Body-->
            <div class="modal-body">
                <div class="md-form">
                    <i class="fa fa-envelope prefix"></i>
                    <input type="email" name="auth-email" class="form-control">
                    <label for="form2">Ваш email</label>
                </div>

                <div class="md-form">
                    <i class="fa fa-lock prefix"></i>
                    <input type="password" name="auth-password" class="form-control">
                    <label for="form3">Ваш пароль</label>
                </div>
                <div class="text-xs-center">
                    <button class="btn btn-primary btn-lg auth-btn">Авторизоваться</button>
                </div>
            </div>

            <!--Footer-->
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
        <!--/.Content-->
    </div>
</div>

<!-- Modal Register -->
<div class="modal fade modal-ext" id="modal-register" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <!--Content-->
        <div class="modal-content">
            <!--Header-->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h3><i class="fa fa-user"></i> Регистрация</h3>
            </div>
            <!--Body-->
            <div class="modal-body">

                <div class="md-form">
                    <i class="fa fa-user prefix"></i>
                    <input type="text" name="reg-fio" class="form-control">
                    <label for="form2">Ваше ФИО</label>
                </div>

                <div class="md-form">
                    <i class="fa fa-envelope prefix"></i>
                    <input type="email" name="reg-email" class="form-control">
                    <label for="form2">Ваш email</label>
                </div>

                <div class="md-form">
                    <i class="fa fa-lock prefix"></i>
                    <input type="password" name="password-reg" class="form-control">
                    <label for="form3">Ваш пароль</label>
                </div>

                <div class="md-form">
                    <i class="fa fa-lock prefix"></i>
                    <input type="password" name="repeat-password-reg" class="form-control">
                    <label for="form4">Повторите ввод пароля</label>
                </div>

                <div class="text-xs-center">
                    <button class="btn btn-primary btn-lg btn-reg">Регистрация</button>
                </div>
            </div>
            <!--Footer-->
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
        <!--/.Content-->
    </div>
</div>

<!-- Developer Modal Window -->

<div class="modal fade modal-ext" id="modal-developer" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <!--Content-->
        <div class="modal-content">

            <!--Header-->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h3>Разработчик</h3>
            </div>

            <!--Body-->
            <div class="modal-body">
                <div>
                    <img src="<?= $url?>/img/jE1xIetmPXM.jpg" alt="Разработчик" title="Арсен Арустамян - разработчик" class="rounded-circle author-photo" style="width: 200px;" />
                </div>
                <h3 class="author-name">Арсен Арустамян</h3>
                <p class="author-job">Mail.ru Group</p>
                <p class="author-position">
                    Эксперт в области разработки высоконагруженных проектов.<br>
                    Ведущий разработчик.
                </p>
            </div>

            <!--Footer-->
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
        <!--/.Content-->
    </div>
</div>