<?php

    require_once "../service/CartService.php";

    $cartService = new CartService();

    $count = $_GET['count'];
    $id = $_GET['id'];

    $result = $cartService->updateCountProductInCart($id, $count);

    echo $result;
?>