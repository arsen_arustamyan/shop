<?php

    session_start();
    require_once ("../service/CartService.php");

    $cartService = new CartService();

    $result = $cartService->addProductInCart(htmlspecialchars($_GET['id']));

    echo $result;
?>