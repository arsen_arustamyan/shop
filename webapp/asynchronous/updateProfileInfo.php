<?php

    require_once('../service/ProfileService.php');

    $profileService = new ProfileService();

    $info = $_POST['info'];
    $phone = $_POST['phone'];
    $email = $_POST['email'];
    $city = $_POST['city'];
    $street = $_POST['street'];
    $house = $_POST['house'];
    $apartment = $_POST['apartment'];

    $result = $profileService->updateProfileInfo($info, $email, $phone, $city, $street, $house, $apartment);

    echo $result;
?>