<?php

    require_once ("../service/ProductService.php");

    $productService = new ProductService();

    $result = $productService->deleteProduct(htmlspecialchars($_GET['id_product']));

    echo $result;
?>