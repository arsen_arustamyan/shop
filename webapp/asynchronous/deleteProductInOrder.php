<?php

    require_once ("../service/OrderService.php");

    $orderService = new OrderService();

    $result = $orderService->deleteProductInOrder(htmlspecialchars($_GET['id_product']), htmlspecialchars($_GET['id_order']));

    echo $result;
?>
