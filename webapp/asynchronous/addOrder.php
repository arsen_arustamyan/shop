<?php

    session_start();
    require_once ('../service/OrderService.php');

    $orderService = new OrderService();

    $comment = $_POST['comment'];
    $deliveryMethod = $_POST['delivery_method'];

    $result = $orderService->addOrder($deliveryMethod, $comment);

    echo $result;
?>

