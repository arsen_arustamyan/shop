<?php

    session_start();
    require_once ("DataBase.php");

    class UserService
    {

        //Получение всех пользователей
        public function getUsers()
        {
            $db = new DataBase();
            $users = $db->select("SELECT id, surname, name, middle_name, email, phone FROM users");
            $db->close();
            return $users;
        }

        //Получение информации о пользователе по id
        public function getUserById(int $id)
        {
            $db = new DataBase();
            $user = $db->select("SELECT * FROM users WHERE id = '$id'");
            $db->close();
            return $user[0];
        }

        //Удаление пользователя по id
        public function deleteUser($id)
        {
            $db = new DataBase();
            $result = $db->delete("DELETE FROM users WHERE id = '$id'");
            $db->close();
            return $result;
        }

    }
?>