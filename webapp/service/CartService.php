<?php

    session_start();
    require_once ("DataBase.php");

    class CartService
    {

        //Добавление товара в корзину
        public function addProductInCart(int $id): bool
        {
            $flag = false;
            if (isset($_SESSION['login'])) {
                $count = $this->getProductInCartByIdProduct($id);
                $user_id = $_SESSION['login'];

                if ($count > 0) {
                    $db = new DataBase();
                    $count_product = (int)$db->select("SELECT count_product FROM cart 
                                                        WHERE id_product = '$id' 
                                                        AND user_id = (SELECT id FROM users WHERE email = '$user_id')")[0]["count_product"];
                    $count_product++;
                    $result = $this->updateCountProductInCart($id, $count_product);
                    $db->close();
                    if ($result > 0)
                        $flag = true;
                } else {
                    $db = new DataBase();
                    $result = $db->insert("INSERT INTO cart VALUES ('', '$id', (SELECT id FROM users WHERE email='$user_id'), '1')");
                    $db->close();
                    if ($result > 0)
                        $flag = true;
                }
            }
            return $flag;
        }

        //Получение товаров пользователя, хранящихся в корзине
        public function getProductsInCart()
        {
            $result = null;
            if (isset($_SESSION['login'])) {
                $db = new DataBase();
                $user_id = $_SESSION['login'];
                $sqlQuery = "SELECT pr.id, pr.name, pr.cena, i.path, c.count_product FROM product pr 
                            INNER JOIN cart c ON c.id_product = pr.id 
                            INNER JOIN images i ON pr.id = i.product_id 
                            WHERE c.user_id = (SELECT id FROM users WHERE email='$user_id') GROUP BY (pr.id)";
                $result = $db->select($sqlQuery);
                $db->close();
            }
            return $result;
        }

        //Получение значения поля count из таблицы cart
        public function getProductInCartByIdProduct(int $id): int
        {
            $db = new DataBase();
            $login = $_SESSION['login'];
            $result = $db->select("SELECT * FROM cart 
                                    WHERE id_product = '$id' 
                                    AND user_id = (SELECT id FROM users WHERE email = '$login')");
            $db->close();
            return count($result);
        }

        //Обновление значение поля count в таблице cart
        public function updateCountProductInCart($id, $count)
        {
            $db = new DataBase();
            $login = $_SESSION['login'];
            $result = $db->update("UPDATE cart SET count_product ='$count' 
                                    WHERE id_product='$id' 
                                    AND user_id = (SELECT id FROM users WHERE email = '$login')");
            $db->close();
            return $result;
        }

        //Удаление товара из корзины по идентификатору
        public function deleteProductById($id)
        {
            $db = new DataBase();
            $login = $_SESSION['login'];
            $result = $db->delete("DELETE FROM cart 
                                    WHERE id_product = '$id' 
                                    AND user_id = (SELECT id FROM users WHERE email = '$login')");
            $db->close();
            return $result;
        }

        //Удаление всех товаров пользователя из корзины
        public function deleteAllProducts()
        {
            $db = new DataBase();
            $login = $_SESSION['login'];
            $db->delete("DELETE FROM cart 
                        WHERE user_id = (SELECT id FROM users WHERE email = '$login')");
            $db->close();
        }
    }

?>