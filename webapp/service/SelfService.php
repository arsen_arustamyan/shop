<?php
    session_start();

    require_once ("DataBase.php");

    class SelfService
    {

        //Реалиазция процесса выхода из личного кабинета
        function logout()
        {
            unset($_SESSION['login']);
        }

        //Реализация процесса авторизации пользователя
        function authUser(string $email, string $password)
        {
            $db = new DataBase();
            $result = $db->select("SELECT * FROM users WHERE email='$email' AND password = '$password'");
            if (count($result) > 0) {
                $_SESSION['login'] = $email;
            }
            $db->close();
            return count($result);
        }

        //Проверка на то что, являетя ли текущий пользователь - администратором
        function isAdmin(): bool
        {
            $isAdmin = false;
            $currentLogin = $_SESSION['login'];
            $db = new DataBase();
            $result = $db->select("SELECT id FROM users WHERE email = '$currentLogin' 
                                    AND role_id = (SELECT id FROM roles WHERE role = 'Admin')");
            if (count($result) > 0)
                $isAdmin = true;
            $db->close();
            return $isAdmin;
        }

        //Проверка на то что, являетя ли текущий пользователь - продавцом
        function isSeller(): bool
        {
            $isSeller = false;
            $currentLogin = $_SESSION['login'];
            $db = new DataBase();
            $result = $db->select("SELECT id FROM users WHERE email = '$currentLogin' 
                                    AND role_id = (SELECT id FROM roles WHERE role = 'Seller')");
            if (count($result) > 0)
                $isSeller = true;
            $db->close();
            return $isSeller;
        }
    }

?>