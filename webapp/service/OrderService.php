<?php

    session_start();

    require_once("DataBase.php");
    require_once("ProfileService.php");
    require_once("CartService.php");

    $cartService = new CartService();
    $profileService = new ProfileService();
    $login = $_SESSION['login'];

    class OrderService
    {

        //Добавление заказа в БД (orders, order_product)
        function addOrder($delivery_method, $comment)
        {
            global $cartService;
            global $login;
            global $profileService;
            $db = new DataBase();
            $price = $profileService->getSumma();
            $current_date = date("Y-m-d H:i:s");
            //Получение id, вставленной записи
            $id_order = $db->lastInsertId("INSERT INTO orders VALUES ('', (SELECT id FROM users WHERE email='$login'), 
                                            (SELECT id FROM status_order WHERE status='Принят'), '$price', '$current_date', '$comment', '$delivery_method')");
            if ($id_order > 0) {
                //Вставка заказа (id(заказа) => id(продукта) в таблицу order_product)
                foreach ($cartService->getProductsInCart() as $product) {
                    $id_product = $product["id"];
                    $count_product = $product["count_product"];
                    //echo $id_order."<br />";
                    $db->insert("INSERT INTO order_product VALUES ('$id_order', '$id_product', '$count_product')");
                }
                $cartService->deleteAllProducts();
                echo json_encode(array("status" => 200));
            } else {
                echo json_encode(array("status" => 500));
            }
        }

        //Получение всех заказов
        function getOrders()
        {
            $db = new DataBase();
            $orders = $db->select("SELECT od.id, od.cena, od.date_add, s.status, u.email FROM orders od 
                                    INNER JOIN status_order s ON s.id = od.status_order 
                                    INNER JOIN users u ON od.user_id = u.id");
            $db->close();
            return $orders;
        }

        //Получение заказа по идентификатору
        function getOrderById(int $id)
        {
            $db = new DataBase();
            $order = $db->select("SELECT p.id AS product_id, 
                                o.id AS order_id, 
                                p.name AS product_name, 
                                p.cena, op.count, 
                                u.surname, u.name AS user_name, 
                                u.middle_name, u.email, 
                                u.phone, u.city, u.street, 
                                u.house, u.apartment, o.comment, 
                                o.delivery_method, so.status, o.cena AS order_price 
                                FROM orders o 
                                INNER JOIN order_product op ON o.id = op.id_order 
                                INNER JOIN product p ON p.id = op.id_product 
                                INNER JOIN users u ON u.id = o.user_id 
                                INNER JOIN status_order so ON so.id = o.status_order 
                                WHERE op.id_order = '$id'");
            $db->close();
            return $order;
        }

        //Получение заказа по идентификатору пользователя
        function getOrderByUserId(int $id)
        {
            $db = new DataBase();
            $orders = $db->select("SELECT id, cena, date_add FROM orders WHERE user_id = '$id'");
            $db->close();
            return $orders;
        }

        //Подсчёт суммы всех заказов пользователя
        function getPriceOrdersById(int $id)
        {
            $price = 0;
            $db = new DataBase();
            $orders = $db->select("SELECT cena FROM orders WHERE user_id = '$id'");
            foreach ($orders as $order) {
                $price = $price + $order["cena"];
            }
            $db->close();
            return $price;
        }

        //Удаление заказа из БД
        function deleteOrder($id)
        {
            $db = new DataBase();
            $result = $db->delete("DELETE FROM orders WHERE id = '$id'");
            $db->close();
            return $result;
        }

        //Удаление товара из заказа по id (продукта)
        function deleteProductInOrder($product_id, $order_id)
        {
            $db = new DataBase();
            $result = $db->delete("DELETE FROM order_product WHERE id_order='$order_id' AND id_product='$product_id'");
            $db->close();
            return $result;
        }

        //Обновление суммы заказа
        function updatePriceOrder($id, $price)
        {
            $db = new DataBase();
            $result = $db->update("UPDATE orders SET cena = '$price' WHERE id = '$id'");
            $db->close();
            return $result;
        }

        function updateOrder()
        {

        }

        function updateOrderStatus($id, $status) {
            $db = new DataBase();
            $result = $db->update("UPDATE orders SET status_order = (SELECT id FROM status_order WHERE status = '$status') WHERE id = '$id'");
            return $result;
        }

        function updateCountProductInOrder($idProduct, $idOrder, $count) {
            $db = new DataBase();
            $result = $db->update("UPDATE order_product SET count = '$count' WHERE id_product = '$idProduct' AND id_order = '$idOrder'");
            return $result;
        }
    }

?>