<?php
    session_start();
    require_once ("DataBase.php");

    class CategoryService
    {
        //Получение всех категории
        public function getAllCategory()
        {
            $db = new DataBase();
            $category = $db->select("SELECT * FROM category");
            $db->close();
            return $category;
        }

        //Добавление новой категории
        public function addCategory($name) {
            $db = new DataBase();
            $category = $db->insert("INSERT INTO category VALUES ('','$name')");
            $db->close();
            return $category;
        }

        //Удаление категории
        public function deleteCategory($category) {
            $db = new DataBase();
            $result = $db->insert("DELETE FROM category WHERE category_name = '$category'");
            $db->close();
            return $result;
        }

        //Изменение имени категории
        public function changeCategoryName($id, $name) {
            $db = new DataBase();
            $result = $db->update("UPDATE category SET category_name = '$name' WHERE id = '$id'");
            $db->close();
            return $result;
        }
    }
?>