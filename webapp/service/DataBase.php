<?php

class DataBase
{
    private $login = "root";
    private $password = "";
    private $host = "localhost";
    private $dbName = "update_diplom";
    private $pdo = null;

    public function __construct()
    {
        try {
            $this->pdo = new PDO("mysql:host=$this->host;dbname=$this->dbName", $this->login, $this->password);
        } catch (PDOException $ex) {
            echo $ex->getMessage();
        }
    }

    public function select($query): array
    {
        $array = array();
        $result = $this->pdo->query($query);
        foreach ($result as $row) {
            $array[] = $row;
        }
        return $array;
    }

    public function lastInsertId($query) : int
    {
        $this->pdo->exec($query);
        return $this->pdo->lastInsertId();
    }

    public function insert($query)
    {
        $affacted_rows = $this->pdo->exec($query);
        return $affacted_rows;
    }

    public function update($query) : int
    {
        $affacted_rows = $this->pdo->exec($query);
        return $affacted_rows;
    }

    public function delete($query) {
        $affacted_rows = $this->pdo->exec($query);
        return $affacted_rows;
    }

    public function close() {
        $this->pdo = null;
    }

    public function selectAPI($query): array
    {
        $array = array();
        $result = $this->pdo->query($query);
        while($row = $result->fetch(PDO::FETCH_ASSOC)) {
            $array[] = $row;
        }
        return $array;
    }
}

?>