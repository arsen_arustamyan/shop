<?php
    require_once('DataBase.php');

    class ProductService
    {

        //Получение списка товаров со статусом NEW
        public function getProductsIsStatusNew(): array
        {
            $sqlQuery = "SELECT p.id, p.name, i.path, p.cena FROM product p  
                          INNER JOIN images i ON p.id = i.product_id
                          INNER JOIN status_product sp ON p.status_id = sp.id 
                          WHERE sp.status = 'New' 
                          AND p.status_id <> '4'
                          GROUP BY p.id";
            $db = new DataBase();
            $result = $db->select($sqlQuery);
            $db->close();
            return $result;
        }

        //Получение списка товаров со статусом HOT
        public function getProductsIsStatusHot(): array
        {
            $sqlQuery = "SELECT p.id, p.name, i.path, p.cena FROM product p 
                          INNER JOIN images i ON p.id = i.product_id
                          INNER JOIN status_product sp ON p.status_id = sp.id 
                          WHERE sp.status = 'Hot' 
                          AND p.status_id <> '4'
                          GROUP BY p.id";
            $db = new DataBase();
            $result = $db->select($sqlQuery);
            $db->close();
            return $result;
        }

        //Получение товара по id
        public function getProductById(int $id)
        {
            $sqlQuery = "SELECT p.name, p.cena, c.category_name, p.id, p.description, i.path FROM product p 
                          INNER JOIN category c ON c.id = p.category_id
                          INNER JOIN images i ON p.id = i.product_id
                          WHERE p.id = '$id'";
            $db = new DataBase();
            $result = $db->select($sqlQuery)[0];
            $db->close();
            return $result;
        }

        //Получение товара по id (Admin-Panel)
        public function getProductByIdAdminPanel(int $id)
        {
            $sqlQuery = "SELECT pr.name, pr.cena, c.category_name, pr.id, pr.description, sp.status FROM product pr 
                          INNER JOIN category c ON c.id = pr.category_id
                          INNER JOIN status_product sp ON pr.status_id = sp.id
                          WHERE pr.id = '$id'";
            $db = new DataBase();
            $result = $db->select($sqlQuery)[0];
            $db->close();
            return $result;
        }

        //Получение списка товаров по категории для отображения (с картинкой)
        public function getProductsByCategoryName(string $categoryName)
        {
            $db = new DataBase();
            $result = $db->select("SELECT p.id, i.path, p.cena, p.name FROM product p 
                                   INNER JOIN images i ON p.id = i.product_id
                                   WHERE p.category_id = (SELECT id FROM category WHERE category_name = '$categoryName')
                                   AND p.status_id <> '4'
                                   GROUP BY (p.id)");
            $db->close();
            return $result;
        }

        //Получение списка товаров по категории
        public function getProductsByCategory(string $categoryName)
        {
            $db = new DataBase();
            $result = $db->select("SELECT id, cena, name FROM product 
                                    WHERE product.category_id = (SELECT id FROM category WHERE category_name = '$categoryName')");
            $db->close();
            return $result;
        }

        //Добавление товара в БД
        public function addProductInDB($name, $description, $price, $category) {
            $db = new DataBase();
            $id = $db->lastInsertId("INSERT INTO product VALUES ('', '$name', '$description', '$price', 
                                   (SELECT id FROM category WHERE category_name = '$category'), '3')");
            $db->close();
            if ($id > 0)
                return $id;
            else
                return 500;
        }

        //Удаление товара по id
        public function deleteProduct($id)
        {
            $db = new DataBase();
            $result = $db->delete("DELETE FROM product WHERE id = '$id'");
            $db->close();
            return $result;
        }

        //Обновление товара
        public function updateProductInfo($id, $name, $decription, $cena, $status) {
            $db = new DataBase();
            $result = $db->update("UPDATE product SET name = '$name', description = '$decription', cena = '$cena', 
                                    status_id = (SELECT id FROM status_product WHERE status='$status') WHERE id = '$id'");
            $db->close();
            return $result;
        }

        //---------------------API---------------------//

        //Получение всех товаров
        public function getProducts() {
            $sqlQuery = "SELECT p.id, p.name, p.description, i.path, p.cena FROM product p 
                          INNER JOIN images i ON p.id = i.product_id 
                          INNER JOIN status_product sp ON p.status_id = sp.id 
                          GROUP BY p.id";
            $db = new DataBase();
            $result = $db->selectAPI($sqlQuery);
            $db->close();
            return $result;
        }
    }
?>