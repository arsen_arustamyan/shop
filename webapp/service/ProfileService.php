<?php
    session_start();

    require_once('DataBase.php');
require_once ('CartService.php');
    $cartService = new CartService();

    class ProfileService
    {

        //Получение информации о пользователе
        function getUserInfo(): array
        {
            $db = new DataBase();
            $login = $_SESSION['login'];
            $result = $db->select("SELECT * FROM users WHERE email = '$login'");
            $db->close();
            return $result[0];
        }

        //Получение общего количества товаров пользователя, находящихся в корзине (таблица cart)
        function getCount()
        {
            $db = new DataBase();
            $login = $_SESSION['login'];

            $result = $db->select("SELECT SUM(count_product) FROM cart 
                                    WHERE user_id = (SELECT id FROM users WHERE email = '$login')")[0]["SUM(count_product)"];
            $db->close();
            if ($result == 0)
                return 0;
            else
                return $result;
        }

        //Получение общей суммы товаров пользователя, находящихся в корзине (таблица cart)
        function getSumma()
        {
            global $cartService;
            $summa = 0;
            $result = $cartService->getProductsInCart();
            foreach ($result as $item) {
                $summa += (int)$item["cena"] * (int)$item["count_product"];
            }
            return $summa;
        }

        //Обновление иформации о пользователе
        function updateProfileInfo($fio, $email, $phone, $city, $street, $house, $apartment): int
        {
            $infos = explode(" ", $fio);
            $currentUserEmail = $_SESSION['login'];

            $db = new DataBase();
            $id_user = $db->select("SELECT id FROM users WHERE email = '$currentUserEmail'")[0]["id"];

            if ($id_user > 0) {
                $result = $db->update("UPDATE `users` SET 
                                        `surname` = '$infos[0]', 
                                        `name` = '$infos[1]', 
                                        `middle_name` = '$infos[2]', 
                                        `email` = '$email', 
                                        `phone` = '$phone', 
                                        `city` = '$city', 
                                        `street` = '$street',
                                        `house` = '$house', 
                                        `apartment` = '$apartment' 
                                        WHERE `id` = '$id_user'");
                $db->close();

                if (count($email) > 0 && $result > 0) {
                    $_SESSION['login'] = $email;
                    return 500;
                }
            }

            return 404;
        }

        //Получение заказов пользователя
        function getOrdersByUser()
        {
            $login = $_SESSION['login'];
            $db = new DataBase();
            $id_user = $db->select("SELECT id FROM users WHERE email = '$login'")[0]["id"];
            $orders = $db->select("SELECT od.id, od.cena, od.date_add, s.status FROM orders od 
                                    INNER JOIN status_order s ON s.id = od.status_order 
                                    WHERE od.user_id = '$id_user'");
            $db->close();
            return $orders;
        }
    }
?>