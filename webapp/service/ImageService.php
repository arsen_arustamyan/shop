<?php

    require_once ('DataBase.php');

    class ImageService
    {

        // Добавления изображения в БД и на сервер 
        public function addImage() {
            $status = null;
            if ($_FILES['file']['error'] > 0) {
                $status = json_encode(array('status' => 'Ошибка при загрузке файла на сервер'));
            }
            else {
                if ($_FILES['file']['type'] == 'image/png' || $_FILES['file']['type'] == 'image/jpeg') {
                    $path = '../../img/'.$_FILES['file']['name'];
                    if (!move_uploaded_file($_FILES['file']['tmp_name'], $path)) {
                        $status = json_encode(array('status' => 'Ошибка при коппировании'));
                    }
                    else {
                        $id_product = stristr($_SERVER['HTTP_REFERER'], 'id=');
                        $id_product = substr($id_product, 3);

                        $pathImageDB = 'img/'.$_FILES['file']['name'];

                        $db = new DataBase();
                        $id_image = $db->lastInsertId("INSERT INTO images VALUES ('', '$pathImageDB', '$id_product')");
                        $db->close();

                        $status = json_encode(array('status' => $path, 'id' => $id_image));
                    }
                }
                else
                    $status = json_encode(array('status' => 'Ошибка, данный тип файла нельзя загружать на сервер'));
            }
            $id = stristr($_SERVER['HTTP_REFERER'], 'id=');
            $id = substr($id, 3);
            return $status;
        }

        // Получения изображения по id
        public function getImageProduct($id) {
            $db = new DataBase();
            $result = $db->select("SELECT i.id, i.path FROM product pr
                                    INNER JOIN images i on i.product_id = pr.id
                                    WHERE pr.id = '$id'");
            $db->close();
            return $result;
        }

        // Удаление изображения товара
        public function deleteImageInDataBase($id)
        {
            $db = new Database();
            $imageName = substr($db->select("SELECT path FROM images
                                WHERE id = '$id';
                                ")[0]['path'], 4);
            $result = $db->delete("DELETE FROM images WHERE id = '$id'");
            $db->close();
            unlink($_SERVER['DOCUMENT_ROOT'].'/img/'.$imageName);
            return $result;
        }
    }

?>
