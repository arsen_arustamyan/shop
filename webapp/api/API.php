<?php

require_once ('../service/DataBase.php');

class API {
    public function addProductCart($id, $login) {
        $flag = false;

        $count = $this->getProductInCartByIdProduct($id,  $login);

        if ($count > 0) {
            $db = new DataBase();
            $count_product = (int)$db->select("SELECT count_product FROM cart 
                                                WHERE id_product = '$id' 
                                                AND user_id = (SELECT id FROM users WHERE email = '$login')")[0]["count_product"];
            $count_product++;
            $result = $this->updateCountProductInCart($id, $count_product, $login);
            $db->close();
            if ($result > 0)
                $flag = true;
        } else {
            $db = new DataBase();
            $result = $db->insert("INSERT INTO cart VALUES ('', '$id', (SELECT id FROM users WHERE email='$login'), '1')");
            $db->close();
            if ($result > 0)
                $flag = true;
        }

        return $flag;
    }

    //Получение значения поля count из таблицы cart
    public function getProductInCartByIdProduct(int $id, $login): int
    {
        $db = new DataBase();
        $result = $db->select("SELECT * FROM cart 
                                WHERE id_product = '$id' 
                                AND user_id = (SELECT id FROM users WHERE email = '$login')");
        $db->close();
        return count($result);
    }

    //Обновление значение поля count в таблице cart
    public function updateCountProductInCart($id, $count, $login)
    {
        $db = new DataBase();
        $result = $db->update("UPDATE cart SET count_product ='$count' 
                                WHERE id_product='$id' 
                                AND user_id = (SELECT id FROM users WHERE email = '$login')");
        $db->close();
        return $result;
    }

    //Получение общего количества товаров пользователя, находящихся в корзине (таблица cart)
    function getCount($login)
    {
        $db = new DataBase();
        //$result = $db->select("SELECT SUM(count_product) FROM cart WHERE user_id = (SELECT id FROM users WHERE email = '$login') AND status_order = (SELECT id FROM status_order WHERE status = 'В корзине')")[0]["SUM(count_product)"][0];
        $result = $db->select("SELECT SUM(count_product) FROM cart 
                                    WHERE user_id = (SELECT id FROM users WHERE email = '$login')")[0]["SUM(count_product)"];
        $db->close();
        if ($result == 0)
            return 0;
        else
            return $result;
    }

    //Получение общей суммы товаров пользователя, находящихся в корзине (таблица cart)
    function getSumma($login)
    {
        $summa = 0;
        $result = $this->getProductsInCart($login);
        foreach ($result as $item) {
            $summa += (int)$item["cena"] * (int)$item["count_product"];
        }
        return $summa;
    }

    //Получение товаров пользователя, хранящихся в корзине
    function getProductsInCart($login)
    {
        $result = null;
        $db = new DataBase();
        $sqlQuery = "SELECT pr.id, pr.name, pr.cena, i.path, c.count_product FROM product pr 
                            INNER JOIN cart c ON c.id_product = pr.id  
                            INNER JOIN images i ON pr.id = i.product_id 
                            WHERE c.user_id = (SELECT id FROM users WHERE email='$login') GROUP BY (pr.id)";
        $result = $db->select($sqlQuery);
        $db->close();
        return $result;
    }
}

?>