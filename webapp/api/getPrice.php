<?php
    function __autoload($classname) {
        $filename = $classname .".php";
        include_once($filename);
    }

    $api = new API();

    echo json_encode(array("price" => $api->getSumma($_GET['login'])));
?>