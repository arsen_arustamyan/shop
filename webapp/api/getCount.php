<?php
    function __autoload($classname) {
        $filename = $classname .".php";
        include_once($filename);
    }

    $api = new API();

    echo json_encode(array("count" => $api->getCount($_GET['login'])));
?>