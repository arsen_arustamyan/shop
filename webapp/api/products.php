<?php
    function __autoload($classname) {
            $filename = "../service/". $classname .".php";
            include_once($filename);
    }

    $productService = new ProductService();

    echo json_encode($productService->getProducts(), JSON_UNESCAPED_UNICODE);

?>