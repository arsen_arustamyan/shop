<?php
    function __autoload($classname) {
        $filename = $classname .".php";
        include_once($filename);
    }

    $api = new API();

    echo json_encode($api->getProductsInCart('krrera@mail.ru'), JSON_UNESCAPED_UNICODE);
?>