<?php
    function __autoload($classname) {
        $filename = "../service/". $classname .".php";
        include_once($filename);
    }

    $selfService = new SelfService();

    echo json_encode(array("response" => $selfService->authUser($_GET['email'], md5($_GET['password']))));
?>