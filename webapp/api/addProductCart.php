<?php
    function __autoload($classname) {
        $filename = $classname .".php";
        include_once($filename);
    }

    $api = new API();

    echo json_encode(array("response" => $api->addProductCart($_GET['id'], $_GET['login'])));
?>