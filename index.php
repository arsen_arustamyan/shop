<?php
    session_start();
    require_once("webapp/service/ProductService.php");
    require_once ("webapp/service/ProfileService.php");
    $productSerice = new ProductService();
    $profileService = new ProfileService();
    $productsNew = $productSerice->getProductsIsStatusNew();
    $productsHot = $productSerice->getProductsIsStatusHot();
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <title>Интернет-магазин</title>

    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.0/css/font-awesome.min.css">

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Material Design Bootstrap -->
    <link href="css/mdb.min.css" rel="stylesheet">

    <link rel="stylesheet" href="css/toastr.min.css">

    <!-- Your custom styles (optional) -->
    <link href="css/style.css" rel="stylesheet">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

    <script type="text/javascript" src="js/noty/packaged/jquery.noty.packaged.min.js"></script>

</head>

<body>

<? include $_SERVER['DOCUMENT_ROOT']."/template/header.php"?>
  
<div class="container my-alerts" style="display: none">
	<div class="alert alert-warning">
  		<!-- Кнопка для закрытия сообщения, созданная с помощью элемента a -->
  		<a href="#" class="close" data-dismiss="alert">×</a>
      <strong>Внимание!</strong> Мы запустили бета версию приложения для проведения покупок в интернет-магазине. <a href="/desktop" target="_blank">Скачать</a>
	</div>
</div>

<!-- Start your project here-->

<div class="my-container carusel">
    <div class="row">
        <!--Carousel Wrapper-->
        <div id="carousel-example-1" class="carousel slide carousel-fade" data-ride="carousel">
            <!--Indicators-->
            <ol class="carousel-indicators">
                <li data-target="#carousel-example-1" data-slide-to="0" class="active"></li>
                <li data-target="#carousel-example-1" data-slide-to="1"></li>
            </ol> <!--/.Indicators--> <!--Slides-->
            <div class="carousel-inner" role="listbox"> <!--First slide-->
                <div class="carousel-item active"><img src="http://mdbootstrap.com/img/Photos/Slides/img (18).jpg"
                                                       alt="First slide"></div>
                <!--/First slide--> <!--Second slide-->
                <div class="carousel-item"><img src="http://mdbootstrap.com/img/Photos/Slides/img (19).jpg"
                                                alt="Second slide"></div>
                <!--/Second slide--> <!--Third slide-->
                <!--/Third slide--> </div> <!--/.Slides--> <!--Controls-->
            <a class="left carousel-control" href="#carousel-example-1" role="button" data-slide="prev">
                <span class="icon-prev" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#carousel-example-1" role="button" data-slide="next">
                <span class="icon-next" aria-hidden="true"></span>
                <span class="sr-only">Next</span> </a> <!--/.Controls-->
        </div>
    </div>
</div>

<div class="container new-product z-depth-3">
    <h3>Новые товары</h3>
    <div class="row">
        <? foreach ($productsNew as $product): ?>
            <div class="col-md-3" onclick="location.href='pages/product.php?id=<?= $product["id"]?>'">
                <div class="card card-cascade narrower">
                    <!--Card image-->
                    <div class="view overlay hm-white-slight">
                        <img src="img/fe_bagenew.png">
                        <img src="<?= $product["path"] ?>" class="img-product" alt="">
                        <a>
                            <div class="mask"></div>
                        </a>
                    </div>
                    <!--/.Card image-->
                    <!--Card content-->
                    <div class="card-block">
                        <h5 class="red-text"><i class="fa fa-money"></i> <?= $product["cena"] ?> руб.</h5>
                        <!--Title-->
                        <h4 class="card-title"><?= $product["name"] ?></h4>
                    </div>
                    <!--/.Card content-->
                </div>
                <!--/.Card-->
            </div>
        <? endforeach; ?>
    </div>
</div>

<div class="container new-product z-depth-3">
    <h3 class="title-sales">Популярные товары</h3>
    <div class="row">
        <? foreach ($productsHot as $product): ?>
            <div class="col-md-3" onclick="location.href='pages/product.php?id=<?= $product["id"]?>'">
                <div class="card card-cascade narrower">
                    <!--Card image-->
                    <div class="view overlay hm-white-slight">
                        <img src="img/fe_bagehot.png" alt="">
                        <img src="<?= $product["path"] ?>" class="img-product" alt="">
                        <a>
                            <div class="mask"></div>
                        </a>
                    </div>
                    <!--/.Card image-->
                    <!--Card content-->
                    <div class="card-block">
                        <h5 class="red-text"><i class="fa fa-money"></i> <?= $product["cena"] ?> руб.</h5>
                        <!--Title-->
                        <h4 class="card-title"><?= $product["name"] ?></h4>
                    </div>
                    <!--/.Card content-->
                </div>
                <!--/.Card-->
            </div>
        <? endforeach; ?>
    </div>
</div>


<!-- /Start your project here-->

<!-- SCRIPTS -->

<script src="/js/lib.js"></script>

<!-- JQuery -->
<script type="text/javascript" src="js/jquery-2.2.3.min.js"></script>

<script src="js/toastr.min.js"></script>

<!-- Bootstrap tooltips -->
<script type="text/javascript" src="js/tether.min.js"></script>

<!-- Bootstrap core JavaScript -->
<script type="text/javascript" src="js/bootstrap.min.js"></script>

<!-- MDB core JavaScript -->
<script type="text/javascript" src="js/mdb.min.js"></script>

<script src="js/auth.js"></script>

<script src="js/register.js"></script>

</body>

</html>
