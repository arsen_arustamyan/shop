<?php
    session_start();

    require_once ("../webapp/service/ProductService.php");
    require_once ("../webapp/service/ProfileService.php");
    require_once ("../webapp/service/ImageService.php");

    $profileService = new ProfileService();
    $productService = new ProductService();
    $imageService = new ImageService();

    $product = $productService->getProductById($_GET['id']);
    $products = $productService->getProductsByCategoryName($product["category_name"]);
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <title><?= $product["name"] ?></title>

    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.0/css/font-awesome.min.css">

    <!-- Bootstrap core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">

    <!-- Material Design Bootstrap -->
    <link href="../css/mdb.min.css" rel="stylesheet">

    <!-- Your custom styles (optional) -->
    <link href="../css/style.css" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="../css/slick.css">
    <link rel="stylesheet" type="text/css" href="../css/slick-theme.css">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js" defer></script>

    <script type="text/javascript" src="../js/noty/packaged/jquery.noty.packaged.min.js" defer></script>

    <script src="../js/toastr.min.js" defer></script>

    <!-- Bootstrap tooltips -->
    <script type="text/javascript" src="../js/tether.min.js" defer></script>

    <!-- Bootstrap core JavaScript -->
    <script type="text/javascript" src="../js/bootstrap.min.js" defer></script>

    <!-- MDB core JavaScript -->
    <script type="text/javascript" src="../js/mdb.min.js" defer></script>

    <script type="text/javascript" src="../js/slick.min.js" defer></script>

    <script src="../js/lib.js" defer></script>

    <script src="../js/addProductInCart.js" defer></script>

    <script src="../js/auth.js" defer></script>

    <script src="../js/register.js" defer></script>

    <script src="../js/promise.min.js"></script>

</head>

<body>

<? include $_SERVER['DOCUMENT_ROOT']."/template/header.php"?>

<div class="container">
    <div class="row">
        <div class="category-page">
            <h3 class="category-name"><?= $product["category_name"]?></h3>
            <span><a href="category.php?category=<?= $product["category_name"]?>" class="category-href">вернуться к катологу</a></span>
        </div>
    </div>
</div>


<!-- Start your project here-->
<div class="container">
    <div class="row">
        <div class="product-page z-depth-3">
            <div class="col-md-4 border">
                <div class="container-img">
                    <img src="../<?= $product["path"] ?>" alt="">
                </div>
            </div>
            <div class="col-md-5">
                <span class="product-title"><?= $product["name"] ?></span>
                <div class="border-product"></div>
                <span class="product-description">
                    <?= $product["description"]?>
                </span>
            </div>
            <div class="col-md-3">
                <div class="product-price">
                    <span class="price"><?= $product["cena"]?> руб.</span>
                    <span class="status-product"><img src="../img/ok.png" alt=""> есть в наличии</span>
                </div>
                <div class="product-purchase">
                    <span class="page-by">
                        <button type="button" class="btn btn-primary btn-add" datafld="<?= $product["id"] ?>">Купить</button>
                    </span>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="container new-product z-depth-3">
    <h3>Другие товары из категории "<?= $product["category_name"]?>"</h3>
    <div class="row">
        <div class="multiple-items">
            <? foreach ($products as $currentProduct): ?>
            <div class="col-md-3" onclick="location.href = location.protocol + '//' + location.hostname + '/pages/product.php?id=' + <?= $currentProduct['id']?>">
                <div class="card card-cascade narrower">

                    <!--Card image-->
                    <div class="view overlay hm-white-slight">
                        <img src="../<?= $currentProduct["path"] ?>" class="img-fluid img-product" alt="">
                        <a>
                            <div class="mask"></div>
                        </a>
                    </div>
                    <!--/.Card image-->

                    <!--Card content-->
                    <div class="card-block">
                        <h5 class="red-text"><i class="fa fa-money"></i> <?= $currentProduct["cena"]?> руб.</h5>
                        <!--Title-->
                        <h4 class="card-title"><?= $currentProduct["name"]?></h4>
                    </div>
                    <!--/.Card content-->

                </div>
            </div>
            <? endforeach; ?>
        </div>
    </div>
</div>

</body>

</html>
