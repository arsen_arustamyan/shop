<?php
    session_start();

    require_once("../../webapp/service/SelfService.php");

    $selfService = new SelfService();

    if ($selfService->isAdmin()) {
        require_once("../../webapp/service/ProductService.php");
        require_once("../../webapp/service/ImageService.php");

        $productService = new ProductService();
        $imageService = new ImageService();
        $currentProduct = $productService->getProductByIdAdminPanel(htmlspecialchars($_GET['id']));
        $images = $imageService->getImageProduct(htmlspecialchars($_GET['id']));
    } else {
        header($_SERVER['SERVER_PROTOCOL']." 404 Not Found");
        include($_SERVER['DOCUMENT_ROOT'] . "/404.html");
        exit;
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <title>Заказы</title>

    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.0/css/font-awesome.min.css">

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Material Design Bootstrap -->
    <link href="css/mdb.min.css" rel="stylesheet">

    <!-- Your custom styles (optional) -->
    <link href="css/style.css" rel="stylesheet">

    <!-- JQuery -->
    <script type="text/javascript" src="js/jquery-3.1.1.min.js"></script>
    <script type="text/javascript" src="../../js/noty/packaged/jquery.noty.packaged.min.js" defer></script>

    <script src="../../js/admin-panel/addImageProduct.js" defer></script>
    <script src="../../js/admin-panel/deleteImageProduct.js" defer></script>
    <script src="../../js/admin-panel/updateProductInfo.js" defer></script>
    <script src="../../js/admin-panel/deleteProduct.js" defer></script>
    <script src="../../js/lib.js" defer></script>

    <style>
        .uploaded-photo {
            display: block;
            margin: 10px auto;
            justify-content: center;
            align-items: center;
            background-color: #0b51c5;
            color: white;
        }

        .uploaded-photo:hover {
            border: 1px solid #0b51c5;
            background-color: white;
            color: #0b51c5;
            transition: background-color 1s, color 2s;
        }

        .input_text {
            cursor: pointer;
            display: block;
            padding: 10px 40px;
        }

        .my_label {
            font-size: 14px;
            margin-bottom: 0;
        }

        input[type=file] {
            display: none;
        }
    </style>
</head>

<body>
<input type="hidden" name="status_pr" value="<?= $currentProduct['status'] ?>">
<input type="hidden" name="id_pr" value="<?= $_GET['id'] ?>">
<!--Navbar-->
<nav class="navbar navbar-toggleable-md navbar-dark bg-primary">
    <div class="container">
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#collapseEx12" aria-controls="collapseEx2" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <a class="navbar-brand" href="#">Shop</a>
        <div class="collapse navbar-collapse" id="collapseEx12">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="orders.php">Заказы</a>
                </li>
                <? if ($selfService->isAdmin()): ?>
                    <li class="nav-item">
                        <a class="nav-link" href="users.php">Пользователи</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="items.php">Товары</a>
                    </li>
                <? endif; ?>
            </ul>
        </div>
    </div>
</nav>
<!--/.Navbar-->

<div class="container white-container">
    <div class="row">
        <div class="container">
            <h3>Информация о товаре</h3>
        </div>
    </div>
    <div class="row">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="md-form col-12">
                        <div class="md-form">
                            <input type="text" id="form1" class="form-control" name=product_name value="<?= $currentProduct['name']; ?>">
                            <label for="form1" class="">Название товара:</label>
                        </div>
                    </div>
                    <div class="md-form col-12">
                        <div class="md-form">
                            <input type="text" id="form1" class="form-control" name=product_price value="<?= $currentProduct['cena']; ?>">
                            <label for="form1" class="">Цена товара:</label>
                        </div>
                    </div>
                    <div class="md-form col-12">
                        <div class="md-form">
                            <textarea type="text" id="form1" class="md-textarea desc" ><?= $currentProduct['description']; ?></textarea>
                            <label for="form1" class="">Описание товара:</label>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="product_status_block">
                        <p>Бейджик:</p>
                        <input type="radio" name="option1" value="Отсутствует"><span class="radio_text"> Отсутствует</span><br>
                        <input type="radio" name="option1" value="New"><span
                            class="radio_text"> NEW</span><br>
                        <input type="radio" name="option1" value="Hot"><span
                            class="radio_text"> HOT</span><br>
                        <input type="radio" name="option1" value="Sale"><span
                            class="radio_text"> SALE</span><br>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <!-- Indicates a successful or positive action -->
                    <button type="button" class="btn btn-success save_sub">Сохранить</button>
                </div>
                <div class="col-md-6">
                    <!-- Indicates a successful or positive action -->
                    <button style="float: right; display: block" type="button" class="btn btn-danger del_sub">Удалить</button>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container white-container">
    <div class="row">
        <div class="container">
            <h3>ФОТОГРАФИИ ТОВАРА </h3>
        </div>
    </div>
    <div class="row">
        <div class="container">
            <div class="row img-container">
                <? foreach ($images as $image): ?>
                    <div class="col-md-3">
                        <img src="../../<?= $image['path']?>" style="max-width: 100%"/>
                        <button type="button" class="btn btn-outline-danger waves-effect dfs_del" style="display: block; margin: 10px auto" datafld="<?= $image['id'] ?>">Удалить</button>
                    </div>
                <? endforeach; ?>
            </div>
            <div class="row">
                <div class="container">
                    <div class="row">
                        <span class="uploaded-photo">
                            <label for="file-label" class="my_label">
                                <input type="file" name="file" id="file-label">
                                <span class="input_text">Загрузить фото</span>
                            </label>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- SCRIPTS -->

<!-- Bootstrap tooltips -->
<script type="text/javascript" src="js/tether.min.js"></script>

<!-- Bootstrap core JavaScript -->
<script type="text/javascript" src="js/bootstrap.min.js"></script>

<!-- MDB core JavaScript -->
<script type="text/javascript" src="js/mdb.min.js"></script>

</body>

</html>
