<?php
    session_start();

    require_once("../../webapp/service/SelfService.php");

    $selfService = new SelfService();

    if ($selfService->isAdmin() || $selfService->isSeller()) {
        require_once ('../../webapp/service/OrderService.php');

        $orderService = new OrderService();
        $id = htmlspecialchars($_GET['id']);
        $order = $orderService->getOrderById($id);
    } else {
        header($_SERVER['SERVER_PROTOCOL']." 404 Not Found");
        include($_SERVER['DOCUMENT_ROOT'] . "/404.html");
        exit;
    }
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="x-ua-compatible" content="ie=edge">

        <title>Заказы</title>

        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.0/css/font-awesome.min.css">

        <!-- Bootstrap core CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet">

        <!-- Material Design Bootstrap -->
        <link href="css/mdb.min.css" rel="stylesheet">

        <!-- Your custom styles (optional) -->
        <link href="css/style.css" rel="stylesheet">

        <!-- JQuery -->
        <script type="text/javascript" src="js/jquery-3.1.1.min.js"></script>
        <script type="text/javascript" src="../../js/noty/packaged/jquery.noty.packaged.min.js"></script>
        <script src="../../js/admin-panel/orderService.js" defer></script>
        <script src="../../js/lib.js" defer></script>
    </head>

    <body>

    <!--Navbar-->
    <nav class="navbar navbar-toggleable-md navbar-dark bg-primary">
        <div class="container">
            <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#collapseEx12" aria-controls="collapseEx2" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <a class="navbar-brand" href="#">Shop</a>
            <div class="collapse navbar-collapse" id="collapseEx12">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="orders.php">Заказы</a>
                    </li>
                    <? if ($selfService->isAdmin()): ?>
                        <li class="nav-item">
                            <a class="nav-link" href="users.php">Пользователи</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="items.php">Товары</a>
                        </li>
                    <? endif; ?>
                </ul>
            </div>
        </div>
    </nav>
    <!--/.Navbar-->

    <div class="container white-container">
        <div class="row">
            <div class="container">
                <h3>Заказ №<span class="idOrder"><?= $_GET['id']?></span> (<?= $order[0]["status"]?>)</h3>
            </div>
        </div>
        <div class="row">
            <div class="container">
                <table class="table">
                    <thead>
                        <tr>
                            <th>СОДЕРЖИМОЕ ЗАКАЗА</th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody class="products-container">
                        <? foreach ($order as $currentOrder): ?>
                            <tr class="product_<?= $currentOrder['product_id']; ?>">
                                <td>
                                    <?= $currentOrder["product_name"]?>
                                </td>
                                <td>
                                    <?= $currentOrder["cena"]?> руб.
                                </td>
                                <td>
                                    <div class="md-form">
                                        <input type="text" value="<?= $currentOrder["count"] ?>" style="max-width: 40px; text-align: center" class="countProductInOrder">
                                    </div>
                                </td>
                                <td class="itog-price">
                                    <?= (int)$currentOrder["cena"] * (int)$currentOrder["count"]?> руб.
                                </td>
                                <td>
                                    <a href="#" class="delete" datafld="<?= $currentOrder['product_id']; ?>;<?= $currentOrder['order_id']; ?>">убрать из заказа</a>
                                </td>
                            </tr>
                        <? endforeach; ?>
                    </tbody>
                </table>
                <h3 style="float: right"><span style="font-style: italic">Итоговая сумма:</span> <span class="price-order"><?= $currentOrder['order_price']?></span> руб.</h3>
            </div>
        </div>
    </div>

    <div class="container white-container">
        <div class="row">
            <div class="container">
                <h3>Информация о заказе</h3>
            </div>
        </div>
        <div class="row">
            <div class="container">
                <div class="row">
                    <div class="col-md-4">
                        <div class="md-form col-12">
                            <div class="md-form">
                                <input type="text" id="form1" class="form-control" value="<?= $currentOrder["surname"]." ".$currentOrder["user_name"]." ".$currentOrder["middle_name"]?>">
                                <label for="form1" class="">Контактное лицо (ФИО):</label>
                            </div>
                        </div>
                        <div class="md-form col-12">
                            <div class="md-form">
                                <input type="text" id="form1" class="form-control" value="<?= $currentOrder["phone"]?>">
                                <label for="form1" class="">Контактный телефон:</label>
                            </div>
                        </div>
                        <div class="md-form col-12">
                            <div class="md-form">
                                <input type="text" id="form1" class="form-control" value="<?= $currentOrder["email"]?>">
                                <label for="form1" class="">Email:</label>
                            </div>
                        </div>
                        <div class="md-form col-12">
                            <div class="md-form">
                                <textarea type="text" id="form1" class="md-textarea" ><?= $currentOrder["comment"]?></textarea>
                                <label for="form1" class="">Комментарий к заказу:</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="md-form col-12">
                            <div class="md-form">
                                <input type="text" id="form1" class="form-control" value="<?= $currentOrder["city"]?>">
                                <label for="form1" class="">Город:</label>
                            </div>
                        </div>
                        <div class="md-form col-12">
                            <div class="md-form">
                                <input type="text" id="form1" class="form-control" value="<?= $currentOrder["street"]?>">
                                <label for="form1" class="">Улица:</label>
                            </div>
                        </div>
                        <div class="md-form col-12">
                            <div class="md-form">
                                <input type="text" id="form1" class="form-control" value="<?= $currentOrder["house"]?>">
                                <label for="form1" class="">Дом:</label>
                            </div>
                        </div>
                        <div class="md-form col-12">
                            <div class="md-form">
                                <input type="text" id="form1" class="form-control" value="<?= $currentOrder["apartment"]?>">
                                <label for="form1" class="">Квартира:</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="md-form col-12">
                            <div class="md-form">
                                <input type="text" id="form1" class="form-control" value="<?= $currentOrder["delivery_method"]?>">
                                <label for="form1" class="">Способ доставки:</label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <button type="button" class="btn btn-outline-danger waves-effect delete-order" datafld="<?= $_GET['id']?>">Отменить заказ</button>
                </div>
            </div>
        </div>
    </div>

    <!-- SCRIPTS -->

    <!-- Bootstrap tooltips -->
    <script type="text/javascript" src="js/tether.min.js"></script>

    <!-- Bootstrap core JavaScript -->
    <script type="text/javascript" src="js/bootstrap.min.js"></script>

    <!-- MDB core JavaScript -->
    <script type="text/javascript" src="js/mdb.min.js"></script>

    </body>

</html>