<?php
    session_start();

    require_once("../../webapp/service/SelfService.php");

    $selfService = new SelfService();

    if ($selfService->isAdmin() || $selfService->isSeller()) {
        require_once ("../../webapp/service/OrderService.php");
        $orderService = new OrderService();
        $orders = $orderService->getOrders();
    } else {
        header($_SERVER['SERVER_PROTOCOL']." 404 Not Found");
        include($_SERVER['DOCUMENT_ROOT'] . "/404.html");
        exit;
    }
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="x-ua-compatible" content="ie=edge">

        <title>Заказы</title>

        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.0/css/font-awesome.min.css">

        <!-- Bootstrap core CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet">

        <!-- Material Design Bootstrap -->
        <link href="css/mdb.min.css" rel="stylesheet">

        <!-- Your custom styles (optional) -->
        <link href="css/style.css" rel="stylesheet">

        <script type="text/javascript" src="js/jquery-3.1.1.min.js"></script>
        <script src="../../js/admin-panel/changeStatusOrder.js" defer></script>
        <script src="../../js/lib.js" defer></script>
    </head>

    <body>

        <!--Navbar-->
        <nav class="navbar navbar-toggleable-md navbar-dark bg-primary">
            <div class="container">
                <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#collapseEx12" aria-controls="collapseEx2" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <a class="navbar-brand" href="#">Shop</a>
                <div class="collapse navbar-collapse" id="collapseEx12">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item active">
                            <a class="nav-link" href="orders.php">Заказы</a>
                        </li>
                        <? if ($selfService->isAdmin()): ?>
                            <li class="nav-item">
                                <a class="nav-link" href="users.php">Пользователи</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="items.php">Товары</a>
                            </li>
                        <? endif; ?>
                    </ul>
                </div>
            </div>
        </nav>
        <!--/.Navbar-->

        <div class="container white-container">
            <div class="row">
                <div class="container">
                    <h1>Заказы</h1>
                </div>
            </div>
            <div class="row">
                <div class="container">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Пользователь</th>
                            <th>Статус</th>
                            <th>Сумма</th>
                            <th>Время заказа</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach (array_reverse($orders) as $order): ?>
                            <tr datafld="<?= $order["id"] ?>">
                                <th scope="row"><?= $order["id"]?></th>
                                <td><?= $order["email"]?></td>
                                <td>
                                    <div class="btn-group">
                                        <button class="btn btn-primary dropdown-toggle current-status" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?= $order["status"]?></button>

                                        <div class="dropdown-menu">
                                            <a class="dropdown-item">принят</a>
                                            <a class="dropdown-item">отгружен</a>
                                            <a class="dropdown-item">у курьера</a>
                                            <div class="dropdown-divider">доставлен</div>
                                            <a class="dropdown-item">отмена</a>
                                        </div>
                                    </div>
                                </td>
                                <td><?= $order["cena"]?> руб</td>
                                <td><?= date("d.m.Y в H:i", strtotime($order["date_add"])) ?></td>
                                <td><a href="order_information.php?id=<?= $order['id'] ?>">просмотр</a></td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    <!-- SCRIPTS -->

    <!-- JQuery -->
    <script type="text/javascript" src="js/jquery-3.1.1.min.js"></script>

    <!-- Bootstrap tooltips -->
    <script type="text/javascript" src="js/tether.min.js"></script>

    <!-- Bootstrap core JavaScript -->
    <script type="text/javascript" src="js/bootstrap.min.js"></script>

    <!-- MDB core JavaScript -->
    <script type="text/javascript" src="js/mdb.min.js"></script>


    </body>

</html>