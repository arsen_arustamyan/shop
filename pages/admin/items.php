<?php
    session_start();
    require_once("../../webapp/service/SelfService.php");

    $selfService = new SelfService();

    if ($selfService->isAdmin()) {
        require_once ("../../webapp/service/CategoryService.php");

        $categoryService = new CategoryService();
        $category = $categoryService->getAllCategory();
    } else {
        header($_SERVER['SERVER_PROTOCOL']." 404 Not Found");
        include($_SERVER['DOCUMENT_ROOT'] . "/404.html");
        exit;
    }
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="x-ua-compatible" content="ie=edge">

        <title>Заказы</title>

        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.0/css/font-awesome.min.css">

        <!-- Bootstrap core CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet">

        <!-- Material Design Bootstrap -->
        <link href="css/mdb.min.css" rel="stylesheet">

        <!-- Your custom styles (optional) -->
        <link href="css/style.css" rel="stylesheet">

        <script type="text/javascript" src="js/jquery-3.1.1.min.js"></script>
        <script type="text/javascript" src="../../js/noty/packaged/jquery.noty.packaged.min.js" defer></script>

        <script src="../../js/admin-panel/items.js" defer></script>
        <script src="../../js/lib.js" defer></script>
    </head>

    <body>

        <!--Navbar-->
        <nav class="navbar navbar-toggleable-md navbar-dark bg-primary">
            <div class="container">
                <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#collapseEx12" aria-controls="collapseEx2" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <a class="navbar-brand" href="#">Shop</a>
                <div class="collapse navbar-collapse" id="collapseEx12">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item active">
                            <a class="nav-link" href="orders.php">Заказы</a>
                        </li>
                        <? if ($selfService->isAdmin()): ?>
                            <li class="nav-item">
                                <a class="nav-link" href="users.php">Пользователи</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="items.php">Товары</a>
                            </li>
                        <? endif; ?>
                    </ul>
                </div>
            </div>
        </nav>
        <!--/.Navbar-->

        <div class="container white-container">
            <div class="row">
                <div class="container">
                    <h3>Товары</h3>
                </div>
            </div>
            <div class="row">
                <div class="container">
                    <table class="table">
                        <thead>
                        <tr>
                            <th colspan="4">Название категории</th>
                        </tr>
                        </thead>
                        <tbody class="items-container">
                            <? foreach ($category as $currentCategory): ?>
                                    <tr>
                                        <td colspan="1">
                                            <img src='../../img/05_items_960.png'/>
                                            <span><?= $currentCategory["category_name"] ?></span>
                                        </td>
                                        <td colspan="3" style="text-align: right">
                                            <a href='items_in_category.php?category=<?= $currentCategory["category_name"] ?>&id=<?= $currentCategory["id"]?>'>просмотр</a>
                                        </td>
                                    </tr>
                            <? endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="row">
                <div class="container">
                    <div class="col-md-12">
                        <div class="md-form">
                            <input type="text" id="form41" class="form-control add_item_input">
                            <label for="form41" class="">Название категории</label>
                        </div>
                    </div>
                    <div class="col-12">
                        <button type="button" class="btn btn-primary add" style="display: block; margin: 10px auto">Добавить категорию</button>
                    </div>
                </div>
            </div>
        </div>

        <!-- SCRIPTS -->

        <!-- Bootstrap tooltips -->
        <script type="text/javascript" src="js/tether.min.js"></script>

        <!-- Bootstrap core JavaScript -->
        <script type="text/javascript" src="js/bootstrap.min.js"></script>

        <!-- MDB core JavaScript -->
        <script type="text/javascript" src="js/mdb.min.js"></script>

    </body>

</html>
