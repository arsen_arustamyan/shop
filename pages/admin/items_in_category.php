<?php
    session_start();

    require_once("../../webapp/service/SelfService.php");

    $selfService = new SelfService();

    if ($selfService->isAdmin()) {
        require_once ("../../webapp/service/ProductService.php");

        $productService = new ProductService();
        $products = $productService->getProductsByCategory(htmlspecialchars($_GET['category']));
    } else {
        header($_SERVER['SERVER_PROTOCOL']." 404 Not Found");
        include($_SERVER['DOCUMENT_ROOT'] . "/404.html");
        exit;
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <title>Заказы</title>

    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.0/css/font-awesome.min.css">

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Material Design Bootstrap -->
    <link href="css/mdb.min.css" rel="stylesheet">

    <!-- Your custom styles (optional) -->
    <link href="css/style.css" rel="stylesheet">

    <script type="text/javascript" src="js/jquery-3.1.1.min.js"></script>
    <script type="text/javascript" src="../../js/noty/packaged/jquery.noty.packaged.min.js" defer></script>
    <script src="../../js/admin-panel/addProductInDataBase.js" defer></script>
    <script src="../../js/admin-panel/deleteCategory.js" defer></script>
    <script src="../../js/admin-panel/changeCategoryName.js" defer></script>
    <script src="../../js/lib.js" defer></script>
</head>

<body>

<!--Navbar-->
<nav class="navbar navbar-toggleable-md navbar-dark bg-primary">
    <div class="container">
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#collapseEx12" aria-controls="collapseEx2" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <a class="navbar-brand" href="#">Shop</a>
        <div class="collapse navbar-collapse" id="collapseEx12">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="orders.php">Заказы</a>
                </li>
                <? if ($selfService->isAdmin()): ?>
                    <li class="nav-item">
                        <a class="nav-link" href="users.php">Пользователи</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="items.php">Товары</a>
                    </li>
                <? endif; ?>
            </ul>
        </div>
    </div>
</nav>
<!--/.Navbar-->

<div class="container white-container">
    <div class="row">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <h3>Товары</h3>
                    <p style="color: #0091ea">Общее количество товаров (<?= count($products) ?>)</p>
                </div>
                <div class="col-md-6">
                    <div class="md-form">
                        <input type="text" id="c_name" class="form-control add_item_input" value="<?= $_GET['category'] ?>">
                        <button type="button" class="btn btn-primary changeCategoryName" style="display: block; margin: 10px auto">переименовать</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="container">
            <div class="col-md-12">
                <div class="row">
                    <button type="button" data-toggle="modal" data-target="#modal-add" class="btn btn-success" style="display: block; margin: 10px auto">Добавить товар</button>
                    <button type="button" class="btn btn-danger delete-category" style="display: block; margin: 10px auto">Удалить категорию</button>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="container">
            <table class="table">
                <thead>
                <tr>
                    <th>Название товара</th>
                    <th>Стоимость</th>
                    <th colspan="2"></th>
                </tr>
                </thead>
                <tbody class="products_c">
                    <?php foreach ($products as $product): ?>
                        <tr>
                            <td>
                                <?= $product["name"] ?>
                            </td>
                            <td>
                                <?= $product["cena"]?> руб.
                            </td>
                            <td colspan="2" style="text-align: right">
                                <a href='item_information.php?id=<?= $product["id"]?>'>просмотр</a>
                            </td>
                        </tr>
                    <? endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<!-- Modal Register -->
<div class="modal fade modal-ext" id="modal-add" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <!--Content-->
        <div class="modal-content">
            <!--Body-->
            <div class="modal-body">

                <div class="md-form">
                    <input type="text" name="product_name" class="form-control" id="p_n">
                    <label for="p_n">Название товара</label>
                </div>

                <div class="md-form">
                    <input type="text" name="product-description" class="form-control" id="p_d">
                    <label for="p_d">Описание товара</label>
                </div>

                <div class="md-form">
                    <input type="text" name="product-price" class="form-control" id="p_p">
                    <label for="p_p">Цена товара</label>
                </div>

                <input type="hidden" name="exampleCategoryName" value="<?= $_GET['category'] ?>">

                <div class="text-xs-center" style="text-align: center">
                    <button class="btn btn-primary btn-lg btn-reg addProduct">Добавить</button>
                </div>
            </div>
            <!--Footer-->
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
        <!--/.Content-->
    </div>
</div>

<input type="hidden" value="<?= $_GET['category'] ?>" class="hidden_category_value">
<input type="hidden" value="<?= $_GET['id'] ?>" class="hidden_id">

<!-- SCRIPTS -->

<!-- Bootstrap tooltips -->
<script type="text/javascript" src="js/tether.min.js"></script>

<!-- Bootstrap core JavaScript -->
<script type="text/javascript" src="js/bootstrap.min.js"></script>

<!-- MDB core JavaScript -->
<script type="text/javascript" src="js/mdb.min.js"></script>

</body>

</html>
