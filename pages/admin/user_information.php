<?php
    session_start();

    require_once("../../webapp/service/SelfService.php");

    $selfService = new SelfService();

    if ($selfService->isAdmin()) {
        require_once ("../../webapp/service/UserService.php");
        require_once ("../../webapp/service/OrderService.php");

        $userService = new UserService();
        $orderService = new OrderService();
        $user = $userService->getUserById(htmlspecialchars($_GET['id']));
        $orders = $orderService->getOrderByUserId(htmlspecialchars($_GET['id']));
        $price = $orderService->getPriceOrdersById(htmlspecialchars($_GET['id']));
    } else {
        header($_SERVER['SERVER_PROTOCOL']." 404 Not Found");
        include($_SERVER['DOCUMENT_ROOT'] . "/404.html");
        exit;
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <title>Заказы</title>

    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.0/css/font-awesome.min.css">

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Material Design Bootstrap -->
    <link href="css/mdb.min.css" rel="stylesheet">

    <!-- Your custom styles (optional) -->
    <link href="css/style.css" rel="stylesheet">

    <!-- JQuery -->
    <script type="text/javascript" src="js/jquery-3.1.1.min.js"></script>
    <script type="text/javascript" src="../../js/noty/packaged/jquery.noty.packaged.min.js"></script>
    <script src="../../js/admin-panel/user.js" defer></script>
    <script src="../../js/lib.js" defer></script>
</head>

<body>

<!--Navbar-->
<nav class="navbar navbar-toggleable-md navbar-dark bg-primary">
    <div class="container">
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#collapseEx12" aria-controls="collapseEx2" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <a class="navbar-brand" href="#">Shop</a>
        <div class="collapse navbar-collapse" id="collapseEx12">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="orders.php">Заказы</a>
                </li>
                <? if ($selfService->isAdmin()): ?>
                    <li class="nav-item">
                        <a class="nav-link" href="users.php">Пользователи</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="items.php">Товары</a>
                    </li>
                <? endif; ?>
            </ul>
        </div>
    </div>
</nav>
<!--/.Navbar-->

<div class="container white-container">
    <div class="row">
        <div class="container">
            <h3>Информация о пользователе </h3>
            <?php
                switch ($user["role_id"])
                {
                    case 1:
                        echo 'Текущая роль: <b style="color: #0b51c5">Администратор</b>';
                        break;
                    case 2:
                        echo 'Текущая роль: <b style="color: #0b51c5">Пользователь</b>';
                        break;
                    case 3:
                        echo 'Текущая роль: <b style="color: #0b51c5">Продавец</b>';
                        break;
                }
            ?>
        </div>
    </div>

    <div class="container white-container">
        <div class="row">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <div class="md-form col-12">
                            <div class="md-form">
                                <input type="text" id="form1" class="form-control" value="<?= $user["surname"]." ".$user["name"]." ".$user["middle_name"] ?>">
                                <label for="form1" class="">Контактное лицо (ФИО):</label>
                            </div>
                        </div>
                        <div class="md-form col-12">
                            <div class="md-form">
                                <input type="text" id="form1" class="form-control" value="<?= $user["phone"]?>">
                                <label for="form1" class="">Контактный телефон:</label>
                            </div>
                        </div>
                        <div class="md-form col-12">
                            <div class="md-form">
                                <input type="text" id="form1" class="form-control" value="<?= $user["email"]?>">
                                <label for="form1" class="">Email:</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="md-form col-12">
                            <div class="md-form">
                                <input type="text" id="form1" class="form-control" value="<?= $user["city"]?>">
                                <label for="form1" class="">Город:</label>
                            </div>
                        </div>
                        <div class="md-form col-12">
                            <div class="md-form">
                                <input type="text" id="form1" class="form-control" value="<?= $user["street"]?>">
                                <label for="form1" class="">Улица:</label>
                            </div>
                        </div>
                        <div class="md-form col-12">
                            <div class="md-form">
                                <input type="text" id="form1" class="form-control" value="<?= $user["house"]?>">
                                <label for="form1" class="">Дом:</label>
                            </div>
                        </div>
                        <div class="md-form col-12">
                            <div class="md-form">
                                <input type="text" id="form1" class="form-control" value="<?= $user["apartment"]?>">
                                <label for="form1" class="">Квартира:</label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container white-container">
        <div class="row">
            <div class="container">
                <h3>История заказов</h3>
            </div>
        </div>
        <div class="row">
            <table class="table">
                <thead>
                <tr>
                    <th>№</th>
                    <th>Сумма</th>
                    <th>Дата оформления</th>
                </tr>
                </thead>
                <tbody class="products-container">
                    <? foreach ($orders as $order): ?>
                        <tr>
                            <td>
                                №<?= $order["id"] ?>
                            </td>
                            <td>
                                <?= $order["cena"] ?> руб.
                            </td>
                            <td>
                                <?= date("d.m.Y в H:i", strtotime($order["date_add"])) ?>
                            </td>
                        </tr>
                    <? endforeach; ?>
                    <tr>
                        <td colspan="3" style="text-align: right">
                            <b>Итоговая сумма:</b> <?= $price ?>
                        </td>
                    </tr>
                </tbody>
            </table>
            <div class="row">
                <button type="button" class="btn btn-outline-danger waves-effect delete-user" datafld="<?= $_GET['id']?>">Удалить пользователя</button>
            </div>
        </div>
    </div>

    <!-- SCRIPTS -->

    <!-- Bootstrap tooltips -->
    <script type="text/javascript" src="js/tether.min.js"></script>

    <!-- Bootstrap core JavaScript -->
    <script type="text/javascript" src="js/bootstrap.min.js"></script>

    <!-- MDB core JavaScript -->
    <script type="text/javascript" src="js/mdb.min.js"></script>


    </body>

</html>
