<?php
    session_start();
    require_once ("../webapp/service/ProfileService.php");
    require_once ("../webapp/service/CartService.php");
    $profileService = new ProfileService();
    $cartService = new CartService();
    $getProducts = $cartService->getProductsInCart();
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <title>Корзина</title>

    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.0/css/font-awesome.min.css">

    <!-- Bootstrap core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">

    <!-- Material Design Bootstrap -->
    <link href="../css/mdb.min.css" rel="stylesheet">

    <!-- Your custom styles (optional) -->
    <link href="../css/style.css" rel="stylesheet">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js" defer></script>

    <script type="text/javascript" src="../js/noty/packaged/jquery.noty.packaged.min.js" defer></script>

    <script src="../js/lib.js" defer></script>

    <script src="../js/addOrder.js" defer></script>

    <!-- Bootstrap tooltips -->
    <script type="text/javascript" src="../js/tether.min.js" defer></script>

    <!-- Bootstrap core JavaScript -->
    <script type="text/javascript" src="../js/bootstrap.min.js" defer></script>

    <!-- MDB core JavaScript -->
    <script type="text/javascript" src="../js/mdb.min.js" defer></script>

    <script src="../js/updateCountProductService.js" defer></script>

    <script src="../js/auth.js" defer></script>

    <script src="../js/register.js" defer></script>

</head>

<body>

<? include $_SERVER['DOCUMENT_ROOT'] . "/template/header.php" ?>

<div class="container">
    <div class="row">
        <div class="category-page">
            <h3 class="category-name">Корзина</h3>
        </div>
    </div>
</div>

<div class="container new-product z-depth-3">
    <div class="row">
        <div class="col-md-12">
            <!--Shopping Cart table-->
            <div class="table-responsive">
                <table class="table product-table">
                    <!--Table head-->
                    <thead>
                    <tr>
                        <th>Товар</th>
                        <th></th>
                        <th>Стоимость</th>
                        <th>Количество</th>
                        <th>Итого</th>
                        <th></th>
                    </tr>
                    </thead>
                    <!--/Table head-->

                    <!--Table body-->
                    <tbody>

                    <? if (isset($_SESSION['login']) && !empty($getProducts)): ?>
                      <? foreach ($getProducts as $product): ?>
                      <tr class="content_good_<?= $product["id"] ?>">
                          <th scope="row">
                              <img src="../<?= $product["path"] ?>" alt="" class="img-fluid img-cart">
                          </th scope="row">
                          <td>
                              <h5><strong><?= $product["name"] ?></strong></h5>
                          </td>
                          <td>
                              <span class="cena_good_<?= $product["id"] ?>">
                                  <?= $product["cena"] ?> руб.
                              </span>
                          </td>
                          <td>
                              <span class="count_<?= $product["id"] ?> prd_count"><?= $product["count_product"] ?></span>
                              <div class="btn-group" data-toggle="buttons">
                                  <label class="btn btn-sm btn-primary btn-rounded" onclick="decrementProduct(this)" datafld="<?= $product["id"] ?>">
                                      <input type="radio" name="options" id="option1">&mdash;
                                  </label>
                                  <label class="btn btn-sm btn-primary btn-rounded" onclick="incrementProduct(this)" datafld="<?= $product["id"] ?>">
                                      <input type="radio" name="options" id="option1">+
                                  </label>
                              </div>
                          </td>
                          <td>
                              <span class="summa_good_<?= $product["id"] ?> price_good">
                                  <?= (int) $product["cena"] * (int) $product["count_product"] ?> руб.
                              </span>
                          </td>
                          <td>
                              <button type="button" class="btn btn-sm btn-primary" onclick="deleteOrder(this)" datafld="<?= $product["id"] ?>">X
                              </button>
                          </td>
                      </tr>
                      <? endforeach; ?>
                    <? endif ?>

                    </tbody>
                    <!--/Table body-->
                </table>
            </div>
            <!--/Shopping Cart table-->
        </div>
    </div>

    <div class="col-md-12">
        <div class="col-md-6">
            <h3>Способ доставки</h3>
            <label for="r1">
                <input type="radio" id="r1" name="rad" value="Курьерская доставка с оплатой при получении">
                <span>Курьерская доставка с оплатой при получении</span>
            </label>
            <label for="r2">
                <input type="radio" id="r2" name="rad" value="Почта России с наложенным платежом">
                <span>Почта России с наложенным платежом</span>
            </label>
            <label for="r3">
                <input type="radio" id="r3" name="rad" value="Доставка через терминалы QIWI POST ">
                <span>Доставка через терминалы QIWI POST </span>
            </label>
        </div>
        <div class="col-md-6">
            <h3>Комментарий к заказу</h3>
            <div class="md-form">
                <i class="fa fa-pencil prefix"></i>
                <textarea type="text" id="form8" class="md-textarea comment"></textarea>
                <label for="form8">Введите ваш комментарий: </label>
            </div>
        </div>
        <div class="itog_price">
            <span class="itog_price_style">Итого: <span class="itog_price_order">
              <? if(isset($_SESSION['login'])): ?>
                <?= $profileService->getSumma() ?> руб.
              <? endif; ?>
            </span>
            <span><a class="btn btn-outline-success btn-rounded waves-effect addOrderButton">Оформить заказ</a></span>
        </div>
    </div>
</div>

</body>

</html>
