<?php
    session_start();
    require_once ("../webapp/service/ProfileService.php");

    if(isset($_SESSION['login'])) {
        require_once ("../webapp/service/ProfileService.php");
        $profileService = new ProfileService();
        $user = $profileService->getUserInfo();
        $orders = $profileService->getOrdersByUser();
    } else {
        header($_SERVER['SERVER_PROTOCOL']." 404 Not Found");
        include($_SERVER['DOCUMENT_ROOT'] . "/404.html");
        exit;
    }
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <title>Личный кабинет</title>

    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.0/css/font-awesome.min.css">

    <!-- Bootstrap core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">

    <!-- Material Design Bootstrap -->
    <link href="../css/mdb.min.css" rel="stylesheet">

    <!-- Your custom styles (optional) -->
    <link href="../css/style.css" rel="stylesheet">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

    <script type="text/javascript" src="../js/noty/packaged/jquery.noty.packaged.min.js"></script>

</head>

<body>

<? include $_SERVER['DOCUMENT_ROOT'] . "/template/header.php" ?>

<div class="container">
    <div class="row">
        <div class="category-page">
            <h3 class="category-name">Личный кабинет</h3>
        </div>
    </div>
</div>

<div class="container new-product z-depth-3">
    <div class="row">
        <div class="col-md-6 profile-info">
            <h2 class="profile-h2">Ваши данные</h2>
            <div class="user-data">
                <div class="md-form">
                    <input type="text" id="form5" name="fio" class="form-control" value="<?= $user["surname"]." ".$user["name"]." ".$user["middle_name"]?>">
                    <label for="form5">Контактное лицо (ФИО):</label>
                </div>
                <div class="md-form">
                    <input type="text" id="form5" name="phone" class="form-control" value="<?= $user["phone"] ?>">
                    <label for="form5">Контактный телефон:</label>
                </div>
                <div class="md-form">
                    <input type="text" id="form5" name="email" class="form-control" value="<?= $user["email"] ?>">
                    <label for="form5">Email адресс:</label>
                </div>
            </div>
            <h2 class="profile-h2">Адресс доставки</h2>
            <div class="user-data">
                <div class="md-form">
                    <input type="text" id="form5" name="city" class="form-control" value="<?= $user["city"] ?>">
                    <label for="form5">Город:</label>
                </div>
                <div class="md-form">
                    <input type="text" id="form5" name="street" class="form-control" value="<?= $user["street"] ?>">
                    <label for="form5">Улица:</label>
                </div>
                <div class="md-form col-md-6">
                    <input type="text" id="form5" name="house" class="form-control" value="<?= $user["house"] ?>">
                    <label for="form5">Дом:</label>
                </div>
                <div class="md-form col-md-6">
                    <input type="text" id="form5" name="apartment" class="form-control" value="<?= $user["apartment"] ?>">
                    <label for="form5">Квартира:</label>
                </div>
            </div>
            <div>
                <a class="btn btn-outline-success btn-rounded waves-effect editBtn">Сохранить</a>
            </div>
            <div class="container_theme_title">
                <span class="changeTheme">Изменить тему оформления</span>
            </div>
            <div class="container_theme">
                <div class="col-md-5 container_theme__jordy_blue_lapis_lazuri_color theme" datafld="jordy_blue__lapis_lazuri_color">
                  <div class="row">
                    <div class="container_theme__jordy_blue_lapis_lazuri_color__top"></div>
                  </div>
                </div>
                <div class="col-md-5 offset-md-1 container_theme__jungle_green_thousand_herb theme" datafld="jungle_green_thousand_herb">
                  <div class="row">
                    <div class="container_theme__jungle_green_thousand_herb__top"></div>
                  </div>
                </div>
                <div class="col-md-5 container_theme__vk theme" datafld="vk">
                  <div class="row">
                    <div class="container_theme__vk__top"></div>
                  </div>
                </div>
                <div class="col-md-5 offset-md-1 container_theme__ok theme" datafld="ok">
                  <div class="row">
                    <div class="container_theme__ok__top"></div>
                  </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 profile-orders-info">
            <h2 class="profile-h2">Ваши заказы</h2>
            <div class="order">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Цена</th>
                            <th>Дата оформления</th>
                            <th>Статус заказа</th>
                        </tr>
                    </thead>
                    <tbody>
                        <? foreach (array_reverse($orders) as $order): ?>
                            <tr>
                                <th scope="row"><?= $order["id"]?></th>
                                <td><?= $order["cena"]?> руб.</td>
                                <td><?= date("d.m.Y в H:i", strtotime($order["date_add"])) ?></td>
                                <td><?= $order["status"]?></td>
                            </tr>
                        <? endforeach; ?>
                    </tbody>
                </table>

            </div>
        </div>
    </div>
</div>

<script src="../js/lib.js"></script>

<script src="../js/updateProfileInfo.js"></script>

<!-- Bootstrap tooltips -->
<script type="text/javascript" src="../js/tether.min.js"></script>

<!-- Bootstrap core JavaScript -->
<script type="text/javascript" src="../js/bootstrap.min.js"></script>

<!-- MDB core JavaScript -->
<script type="text/javascript" src="../js/mdb.min.js"></script>

<script src="../js/auth.js"></script>

<script src="../js/register.js"></script>

</body>

</html>
