<?php
    session_start();
    require_once ("../webapp/service/ProfileService.php");
    require_once("../webapp/service/ProductService.php");
    $profileService = new ProfileService();
    $productService = new ProductService();
    $products = $productService->getProductsByCategoryName($_GET['category']);
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <title><?= $_GET['category'] ?></title>

    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.0/css/font-awesome.min.css">

    <!-- Bootstrap core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">

    <!-- Material Design Bootstrap -->
    <link href="../css/mdb.min.css" rel="stylesheet">

    <!-- Your custom styles (optional) -->
    <link href="../css/style.css" rel="stylesheet">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

</head>

<body>

<? include $_SERVER['DOCUMENT_ROOT'] . "/template/header.php" ?>

<div class="container">
    <div class="row">
        <div class="category-page">
            <h3 class="category-name"><?= $_GET['category'] ?></h3>
        </div>
    </div>
</div>

<div class="container new-product z-depth-3">
    <h3>Товары из категории <?= $_GET['category'] ?></h3>
    <div class="row">
        <div class="multiple-items col-md-12">
            <? foreach ($products as $currentProduct): ?>
                <div class="col-md-3" onclick="location.href='product.php?id=<?= $currentProduct["id"]?>'">
                    <div class="card card-cascade narrower">

                        <!--Card image-->
                        <div class="view overlay hm-white-slight">
                            <img src="../<?= $currentProduct["path"] ?>" class="img-fluid img-product" alt="">
                            <a>
                                <div class="mask"></div>
                            </a>
                        </div>
                        <!--/.Card image-->

                        <!--Card content-->
                        <div class="card-block">
                            <h5 class="red-text"><i class="fa fa-money"></i> <?= $currentProduct["cena"] ?> руб.</h5>
                            <!--Title-->
                            <h4 class="card-title"><?= $currentProduct["name"] ?></h4>
                        </div>
                        <!--/.Card content-->

                    </div>
                </div>
            <? endforeach; ?>
        </div>
    </div>
</div>

<script type="text/javascript" src="../js/noty/packaged/jquery.noty.packaged.min.js"></script>

<script src="../js/lib.js"></script>

<!-- Bootstrap tooltips -->
<script type="text/javascript" src="../js/tether.min.js"></script>

<!-- Bootstrap core JavaScript -->
<script type="text/javascript" src="../js/bootstrap.min.js"></script>

<!-- MDB core JavaScript -->
<script type="text/javascript" src="../js/mdb.min.js"></script>

<script src="../js/auth.js"></script>

<script src="../js/register.js"></script>

</body>

</html>
