"use strict";

(function () {
    window.createPromise = function (url, params, typeXHR) {
        return new Promise(function (resolve) {
            if (typeXHR == "post") {
                $.post(url, params, function (data) {
                    resolve(data);
                });
            } else if (typeXHR == "get") {
                $.get(url, params, function (data) {
                    resolve(data);
                });
            }
        });
    };

    window.showMessage = function (text, typeAlert, typeLayout) {
        var message = noty({
            text: text,
            type: typeAlert,
            dismissQueue: true,
            timeout: 10000,
            closeWith: ['click'],
            layout: typeLayout,
            theme: 'relax',
            maxVisible: 10
        });
    };

    window.installTheme = function () {
        var link = document.createElement('link');
        link.setAttribute('rel', 'stylesheet');
        link.setAttribute('href', JSON.parse(localStorage.bgc).path);
        document.head.appendChild(link);
    };

    if (localStorage.getItem('bgc')) {
        installTheme();
    }
})();