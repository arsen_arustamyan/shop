'use strict';

$(document).ready(function () {

  $('.changeTheme').click(function () {
    $('.container_theme').css({ 'display': 'block' });
  });

  document.querySelector('.container_theme').addEventListener('click', function (ev) {
    if (ev.target.tagName === 'DIV' && ev.target.classList.contains('theme')) {
      var path = ev.target.getAttribute('datafld');
      localStorage.bgc = JSON.stringify({ path: location.href.substring(0, 7) + location.hostname + '/css/themes/' + path + '.css' });
      installTheme();
      window.scrollTo(0, 0);
    }
  });

  $('.editBtn').click(function () {
    var info = $('input[name=fio]').val();
    var phone = $('input[name=phone]').val();
    var email = $('input[name=email]').val();
    var city = $('input[name=city]').val();
    var street = $('input[name=street]').val();
    var house = $('input[name=house]').val();
    var apartment = $('input[name=apartment]').val();

    var promise = createPromise("../webapp/asynchronous/updateProfileInfo.php", { info: info, phone: phone, email: email, city: city, street: street, house: house, apartment: apartment }, "post");

    promise.then(function (data) {
      //alert(data);
      if (data == 500) {
        showMessage("Личные данные, были успешно изменены!", "success", "top");
      } else {
        showMessage("Во время измения личных данных, произошла ошибка", "error", "top");
      }
    });
  });
});