'use strict';

(function () {

    function auth() {
        var email = document.querySelector('input[name=auth-email]').value;
        var password = document.querySelector('input[name=auth-password]').value;

        var promis = createPromise("/webapp/asynchronous/authUser.php", { email: email, password: password }, "post");

        promis.then(function (data) {
            console.log(data);
            if (data == 1) document.location.href = location.href;else {
                showMessage('Неправильный логин/пароль', 'warning', 'topRight');
            }
        });
    }

    function exit() {
        var promis = createPromise("/webapp/asynchronous/exitUser.php", {}, "get");

        promis.then(function (data) {
            return document.location.href = location.href;
        });
    }

    var authBtn = document.querySelector('.auth-btn');
    var exitBtn = document.querySelector('.exit');

    if (authBtn) {
        authBtn.addEventListener('click', function () {
            auth();
        });
    }

    if (exitBtn) {
        exitBtn.addEventListener('click', function () {
            exit();
        });
    }
})();