'use strict';

(function () {

    function auth() {

        var name = document.querySelector('input[name=reg-fio]').value;
        var password = document.querySelector('input[name=password-reg]').value;
        var repeatPassword = document.querySelector('input[name=repeat-password-reg]').value;
        var email = document.querySelector('input[name=reg-email]').value;

        if (isName(name) && isEmail(email) && isIdentityPassword(password, repeatPassword)) {
            var promis = createPromise("../webapp/asynchronous/addUser.php", { name: name, email: email, password: password }, 'post');

            // document.location.href = 'index.php'
            promis.then(function (data) {
                data = JSON.parse(data);
                if (data.status == 200) {
                    showMessage("Вы успешно зарегистрированы!", "success", "topRight");
                    document.location.href = '/index.php';
                } else if (data.status == 500) {
                    showMessage("Ошибка: Произошла ошибка на стороне сервера!", "error", "topRight");
                }
            });
        } else {
            showMessage("Проверьте правильность введеных данных!", "warning", "topRight");
        }
    }

    function isName(name) {
        return name.length > 0;
    }

    function isEmail(email) {
        return email.length > 0;
    }

    function isIdentityPassword(password, repeatPassword) {
        return password == repeatPassword && password.length > 0 && repeatPassword.length > 0;
    }

    document.querySelector('.btn-reg').addEventListener('click', function () {
        auth();
    });
})();