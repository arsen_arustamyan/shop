(function () {
    var changeCategoryNameButton = document.querySelector('.changeCategoryName');

    if (changeCategoryNameButton) {
        changeCategoryNameButton.addEventListener('click', function () {
            var name = document.getElementById('c_name').value;
            var id = document.querySelector('.hidden_id').value;

            $.get('../../webapp/asynchronous/changeCategoryName.php', {id: id, name: name}, function (data) {
                if (data == 1) {
                    document.querySelector('.hidden_category_value').value = name
                    $('input[name=exampleCategoryName]').val(name)

                    showMessage('Название категорий было изменено, вы будете перенапревлены по новому адресу', 'success', 'topRight')

                    setTimeout(function () {
                        location.href = location.protocol + '//' + location.hostname + '/pages/admin/items_in_category.php?category=' + name + '&id=19'
                    }, 2000)

                }
            })

            // document.querySelector('.hidden_category_value').value = name
            // $('input[name=exampleCategoryName]').val(name)
            //
            // console.log('1', document.querySelector('.hidden_category_value').value)
            // console.log('2', $('input[name=exampleCategoryName]').val())
        });
    }
})();
