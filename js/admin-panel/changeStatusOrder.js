(function () {
    var dropdown = document.querySelectorAll('.dropdown-menu');

    if (dropdown.length > 0) {
        dropdown.forEach(function (item) {
            item.addEventListener('click', function (e) {
                e.preventDefault();
                var status = e.target.textContent;
                var id = e.currentTarget.parentNode.parentNode.parentNode.getAttribute('datafld');

                var self = this;

                $.get('../../webapp/asynchronous/updateOrderStatus.php', {status: status, id: id}, function (data) {
                    if (data == 1) {
                        self.parentNode.querySelector('.current-status').textContent = status;
                    }
                });
            })
        })
    }
})();