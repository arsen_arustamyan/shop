'use strict';

(function() {
    var deleteProductButton = document.querySelector('.del_sub');

    if (deleteProductButton) {
        deleteProductButton.addEventListener('click', function() {
            var idProduct = document.querySelector('input[name=id_pr]').value;
            $.get('../../webapp/asynchronous/deleteProductAdminPanel.php', { id_product: idProduct }, function (data) {
                if (data == 1) {
                    alert('Успех');
                } else {
                    alert('Ошибка');
                }
            });
        })
    }
})()