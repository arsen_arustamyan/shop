'use strict';

(function() {
  var imgContainer = document.querySelector('.img-container');

  if (imgContainer) {
    imgContainer.addEventListener('click', function (ev) {
      if (ev.target.tagName == 'BUTTON' && ev.target.classList.contains('dfs_del')) {
        var id = ev.target.getAttribute('datafld');
        var promis = createPromise("../../webapp/asynchronous/deleteImageProduct.php", { id: id }, "get");
        promis.then(function (data) {
          if (data == 1) {
            imgContainer.removeChild(ev.target.parentElement);
          }
        });
      }
    });
  }
})();