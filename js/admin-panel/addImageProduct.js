'use strict';

(function() {
    var inputFile = document.querySelector('input[type=file]');

    if (inputFile) {
        inputFile.addEventListener('change', function () {
            var fd = new FormData();
            fd.append('file', inputFile.files[0]);

            var promis = fetch('../../webapp/asynchronous/addImageProduct.php', { method: 'POST', body: fd });

            promis.then(function (data) {
                return data.json();
            }).then(function (data) {
                console.log(data);
                if (data.status.startsWith('../../')) {
                    var path = data.status;
                    var idImage = data.id;
                    showMessage("Изображение, было успешно загружено на сервер.", "success", "topRight");
                    $('.img-container').append(`
                    <div class="col-md-3">
                        <img src="../../${path}" style="max-width: 100%"/>
                        <button type="button" class="btn btn-outline-danger waves-effect dfs_del" style="display: block; margin: 10px auto" datafld="${idImage}">Удалить</button>
                    </div>
                    `);
                } else {
                    showMessage(data.status, "error", "topRight");
                }
            });
        });
    }
})();