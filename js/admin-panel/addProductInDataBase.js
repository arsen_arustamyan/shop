'use strict';

(function() {
    var clickAddProduct = document.querySelector('.addProduct');

    if (clickAddProduct) {
        clickAddProduct.addEventListener('click', function() {
            var productName = $('input[name=product_name]').val();
            var productDescription = $('input[name=product-description]').val();
            var productPrice = $('input[name=product-price]').val();
            var category = $('input[name=exampleCategoryName]').val();

            console.log(productName, productDescription, productPrice, category)

            addProductInDB(productName, productDescription, productPrice, category)
        })
    }

    function addProductInDB(productName, productDescription, productPrice, category) {
        if (productName.trim().length > 0 && productDescription.trim().length > 0 && productPrice.trim().length > 0 && category.trim().length > 0) {
            $.get('../../webapp/asynchronous/addProduct.php', { name: productName, description: productDescription, price: productPrice, category: category }, function (data) {
                if (data !== 500) {
                    showMessage('\u0422\u043E\u0432\u0430\u0440, <b>' + productName + '</b>, \u0431\u044B\u043B \u0443\u0441\u043F\u0435\u0448\u043D\u043E \u0434\u043E\u0431\u0430\u0432\u043B\u0435\u043D.', 'success', 'topRight');
                    $('.products_c').append(`<tr>
                        <td>${productName}</td>
                        <td>${productPrice} руб.</td>
                        <td colspan="2" style="text-align: right">
                            <a href='item_information.php?id=${data}'>просмотр</a>
                        </td>
                    </tr>
                    `);
                } else {
                    showMessage('В процессе добавления товара - произошла ошибка.', 'error', 'topRight');
                }
            });
        }
    }
})();