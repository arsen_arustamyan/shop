"use strict";

(function () {
    var productsContainer = document.querySelector('.products-container');
    var deleteOrderButton = document.querySelector('.delete-order');
    var countProductInput = document.querySelectorAll('.countProductInOrder');

    if (countProductInput.length > 0) {
        countProductInput.forEach(function (item, index) {
            item.addEventListener('keyup', function (ev) {
                if (ev.keyCode === 8)
                    return false;

                var count = parseInt(this.value);
                this.setAttribute('value', count);
                var idProduct = parseInt(this.parentNode.parentNode.parentNode.className.substr(8));
                var idOrder = parseInt(document.querySelector('.idOrder').textContent);

                var priceProduct = parseInt(this.parentNode.parentNode.parentNode.querySelectorAll('td')[1].textContent);

                var self = this;

                $.get('../../webapp/asynchronous/updateCountProductInOrder.php', {
                    id_order: idOrder,
                    id_product: idProduct,
                    count: count
                }, function (data) {
                    if (data == 1) {
                        self.parentNode.parentNode.parentNode.querySelectorAll('td')[3].textContent = `${count * priceProduct} руб.`;
                        updatePriceAll(idOrder)
                    }
                });
            });
        })
    }

    productsContainer.addEventListener('click', function (ev) {
        if (ev.target.tagName === 'A') {
            var ids = ev.target.getAttribute('datafld').split(";");
            var id_product = ids[0];
            var id_order = ids[1];


            var apiCall = new Promise(function (resolve) {
                $.get("../../webapp/asynchronous/deleteProductInOrder.php", {
                    id_product: id_product,
                    id_order: id_order
                }, function (data) {
                    resolve(data);
                });
            });

            apiCall.then(function (data) {
                if (data == 1) {
                    var container = $('.product_' + id_product);
                    showMessage("Товар был успешно удален из заказа", "success", "topRight");
                    container.detach();
                    updatePriceAll(id_order);
                } else {
                    showMessage("Во время удаления товара из заказа, произошла ошибка", "error", "topRight");
                }
            });
        }
    });

    if (deleteOrderButton) {
        deleteOrderButton.addEventListener('click', function () {
            var id = this.getAttribute("datafld");

            var apiCall = new Promise(function (resolve) {
                $.get("../../webapp/asynchronous/deleteOrder.php", {id: id}, function (data) {
                    resolve(data);
                });
            });

            apiCall.then(function (data) {
                if (data == 1) {
                    showMessage("Заказ был упешно отменен", "success", "topRight");
                } else {
                    showMessage("Во время отменены заказа, произошла ошибка", "error", "topRight");
                }
            });
        })
    }
    //
    function updatePriceAll(id_order) {
        var prices = document.querySelectorAll('.itog-price');
        var price = 0;

        for (var i = 0; i < prices.length; i++) {
            var currentPrice = parseInt(prices[i].innerHTML.trim().replace("руб.", ""));
            price = currentPrice + price;
        }

        var apiCall = new Promise(function (resolve) {
            $.get("../../webapp/asynchronous/updatePriceOrder.php", {id: id_order, price: price}, function (data) {
                resolve(data);
            });
        });

        apiCall.then(function (data) {
            if (data == 1) {
                showMessage("Общая сумма заказа, была обновлена", "success", "topRight");
                $('.price-order').text(price);
            } else {
                alert(2);
            }
        });
    }
})();