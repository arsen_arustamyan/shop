'use strict';

(function () {
    var addItemBtn = document.querySelector('.add');

    if (addItemBtn) {
        addItemBtn.addEventListener('click', function () {
            var itemName = document.querySelector('.add_item_input').value;

            if (itemName == 'название категории') {
                showMessage('Укажите имя новой категории', 'error', 'topRight');
            } else if (itemName.trim().length == 0) {
                showMessage('Имя новой категории, недолжно быть пустой', 'error', 'topRight');
            } else {
                $.get('../../webapp/asynchronous/addCategory.php', {name: itemName}, function (data) {
                    if (data == 1) {
                        showMessage('\u041D\u043E\u0432\u0430\u044F \u043A\u0430\u0442\u0435\u0433\u043E\u0440\u0438\u044F "' + itemName + '", \u0431\u044B\u043B\u0430 \u0443\u0441\u043F\u0435\u0448\u043D\u0430 \u0441\u043E\u0437\u0434\u0430\u043D\u0430', 'success', 'topRight');
                        $('.items-container').append(`
                        <tr>
                            <td colspan="1">
                                <img src='../../img/05_items_960.png'/>
                                <span>${itemName}</span>
                            </td>
                            <td colspan="3" style="text-align: right">
                                <a href='items_in_category.php?category=${itemName}'>просмотр</a>
                            </td>
                        </tr>
                        `
                        );
                    }
                });
            }
        })
    }
})();