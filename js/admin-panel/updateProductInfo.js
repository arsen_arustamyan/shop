'use strict';

(function() {
    var status = document.querySelector('input[name=status_pr]');
    
    $('input[name=option1][value=' + status.value + ']').prop('checked', true);

    var saveProductInfoButton = document.querySelector('.save_sub');

    if (saveProductInfoButton) {
        saveProductInfoButton.addEventListener('click', function() {
            var idProduct = $('input[name=id_pr]').val();
            var productName = $('input[name=product_name]').val();
            var productPrice = $('input[name=product_price]').val();
            var productDescription = $('.desc').val();
            var statusProduct = $('input[name=option1]:checked').val();

            $.get('../../webapp/asynchronous/updateProductInfo.php', { id: idProduct, name: productName, price: productPrice, description: productDescription, status: statusProduct }, function (data) {
                if (data == 1) {
                    console.log('Успех');
                } else {
                    console.log('Ошибка: 505');
                }
            });
        })
    }
})();