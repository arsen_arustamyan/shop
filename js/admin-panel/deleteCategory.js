(function () {
    var deleteCategoryButton = document.querySelector('.delete-category');
    if (deleteCategoryButton) {
        deleteCategoryButton.addEventListener('click', function () {
            var categoryName = document.querySelector('.hidden_category_value').value;

            $.get('../../webapp/asynchronous/deleteCategory.php', {category: categoryName}, function (data) {
                 if (data == 1) {
                     showMessage('Категория была успешна, удалена!', 'success', 'topRight')

                     setTimeout(function () {
                         location.href = location.protocol + '//' + location.hostname + '/pages/admin/items.php';
                     }, 2000)
                 } else {
                     showMessage('Во время удаления категории, произошла ошибка!', 'error', 'topRight')
                 }
            });
        });
    }
})();
