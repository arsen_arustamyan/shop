'use strict';

(function() {
    var deleteUserButton = document.querySelector('.delete-user');

    if (deleteUserButton) {
        deleteUserButton.addEventListener('click', function() {
            var id = this.getAttribute('datafld');

            $.get('../../webapp/asynchronous/deleteUser.php', { id: id }, function (data) {
                if (data == 1) showMessage("Пользователь, был успешно удален", "success", "topRight");else showMessage("Пользователь, не был успешно удален", "error", "topRight");
            });
        })
    }
})();