'use strict';

(function () {

    function setSliderParams() {
        $('.multiple-items').slick({
            infinite: true,
            slidesToShow: 4,
            slidesToScroll: 4
        });
    }

    function updatePrice() {
        var price = document.querySelector('.price').textContent.trim().replace("руб.", "");
        var cartPrice = document.querySelector('.cart-price').textContent.trim().replace("руб.", "");
        return parseInt(price) + parseInt(cartPrice);
    }

    function updateCount() {
        var count = document.querySelector('.count-product').textContent.trim().replace("предмет(а)", "");
        count = parseInt(count);
        return ++count;
    }

    function addProductCart() {
        document.querySelector('.btn-add').addEventListener('click', function () {
            var id = $('.btn-add').attr("datafld");

            var promis = createPromise('../webapp/asynchronous/addProductInCart.php', { id: id }, 'get');

            promis.then(function (data) {
                if (data == 1) {
                    showMessage('\u0422\u043E\u0432\u0430\u0440, \u0431\u044B\u043B \u0443\u0441\u043F\u0435\u0448\u043D\u043E \u0434\u043E\u0431\u0430\u0432\u043B\u0435\u043D \u0432 <i><a href="../pages/shopingcart.php">\u043A\u043E\u0440\u0437\u0438\u043D\u0443.</a></i>', 'success', 'topRight');
                    $('.cart-price').text(updatePrice() + ' \u0440\u0443\u0431.');
                    $('.count-product').text(updateCount() + ' \u043F\u0440\u0435\u0434\u043C\u0435\u0442(\u0430)');
                } else {
                    showMessage('\u0422\u043E\u0432\u0430\u0440, \u043D\u0435 \u0431\u044B\u043B \u0443\u0441\u043F\u0435\u0448\u043D\u043E \u0434\u043E\u0431\u0430\u0432\u043B\u0435\u043D \u0432 <i><a href="../pages/shopingcart.php">\u043A\u043E\u0440\u0437\u0438\u043D\u0443.</a></i>. \u041C\u044B \u0438\u0441\u043F\u0440\u0430\u0432\u043B\u044F\u0435\u043C \u0434\u0430\u043D\u043D\u0443\u044E \u0441\u0438\u0442\u0443\u0430\u0446\u0438\u044E', 'error', 'topRight');
                }
            });
        });
    }

    setSliderParams();

    addProductCart();
})();