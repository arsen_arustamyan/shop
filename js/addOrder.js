'use strict';

(function () {
    function checkout() {
        document.querySelector('.addOrderButton').addEventListener('click', function () {

            var comment = document.querySelector('textarea.comment').value;
            var deliveryMethod = document.querySelector('input[name=rad]:checked');

            if (deliveryMethod == null) {
                return;
            } else {
                deliveryMethod = deliveryMethod.value;
            }

            var priceAll = document.querySelector('.itog_price_order').textContent.trim().replace(' руб', '');
            parseInt(priceAll);

            if (priceAll == 0) {
                showMessage("Корзина покупок пуста!", "warning", "topRight");
            } else {
                showMessage("Ваш заказ, обрабатывается на сервере!<br> " + "Пожалуйста подождите!", "warning", "topRight");

                var promis = createPromise("../webapp/asynchronous/addOrder.php", { comment: comment, delivery_method: deliveryMethod }, 'post');

                promis.then(function (data) {
                    data = JSON.parse(data);
                    if (data.status == 200) {
                        showMessage("Ваш заказ был успешно обработан!<br> " + "Посмотреть информацию о статусе текущего заказа, можно в <b><a href='../pages/profile.php'>Личном кабинете</b>", "success", "topRight");
                    } else if (data == 500) {
                        showMessage("Во время обработки вашего заказа, <b>произошла ошибка</b>. Мы уже её устраняем", "error", "topRight");
                    }
                });
            }
        });
    }

    checkout();
})();