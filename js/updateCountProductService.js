'use strict';

function incrementProduct(input) {
    var id = input.getAttribute("datafld");
    var count = parseInt(document.querySelector('.count_' + id).textContent);
    count++;
    var cena = document.querySelector('.cena_good_' + id).textContent.trim().replace(" руб.", "");

    var promis = createPromise("../webapp/asynchronous/updateCountProduct.php", { id: id, count: count }, 'get');

    promis.then(function (data) {
        if (data == 1) {
            document.querySelector('.count_' + id).textContent = count;
            var priceProduct = parseInt(cena) * parseInt(count);
            document.querySelector(".summa_good_" + id).textContent = priceProduct + ' \u0440\u0443\u0431.';
            updatePriceAll();
            updateCount();
        }
    });
}

function decrementProduct(input) {
    var id = input.getAttribute("datafld");
    var count = parseInt(document.querySelector(".count_" + id).textContent);
    count--;
    var cena = document.querySelector('.cena_good_' + id).textContent.trim().replace(" руб.", "");

    var promis = createPromise("../webapp/asynchronous/updateCountProduct.php", { id: id, count: count }, 'get');

    promis.then(function (data) {
        if (data == 1) {
            document.querySelector('.count_' + id).textContent = count;
            var priceProduct = parseInt(cena) * parseInt(count);
            document.querySelector(".summa_good_" + id).textContent = priceProduct + ' \u0440\u0443\u0431.';
            updatePriceAll();
            updateCount();
        }
    });
}

function deleteOrder(input) {
    var id = input.getAttribute("datafld");

    var promis = createPromise("../webapp/asynchronous/deleteProduct.php", { id: id }, 'get');

    promis.then(function (data) {
        if (data == 1) {
            var container = $('.content_good_' + id);
            container.detach();
            updatePriceAll();
            updateCount();
        }
    });
}

function updatePriceAll() {
    var prices = document.querySelectorAll('.price_good');
    var price = 0;

    for (var i = 0; i < prices.length; i++) {
        var currentPrice = parseInt(prices[i].innerHTML.trim().replace("руб.", ""));
        price = currentPrice + price;
    }

    document.querySelector('.itog_price_order').textContent = price + ' \u0440\u0443\u0431.';
    document.querySelector('.cart-price').textContent = price + ' \u0440\u0443\u0431.';
}

function updateCount() {
    var counts = document.querySelectorAll('.prd_count');
    var count = 0;

    for (var i = 0; i < counts.length; i++) {
        var currentPrice = parseInt(counts[i].innerHTML);
        count = currentPrice + count;
    }

    document.querySelector('.count-product').textContent = count + ' \u043F\u0440\u0435\u0434\u043C\u0435\u0442(\u0430)';
}